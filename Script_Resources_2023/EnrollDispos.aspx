﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="EnrollDispos.aspx.cs" Inherits="Script_Resources_2023.EnrollDispos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
    Enrollment Call
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
    Enrollment Dispositions
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <p>
        <button id="QR_Button_43" amcatgroupattributes="<AmcatGroup Type='QuickResult' ResultDescription='AIA: Test Call' ResultCode='4693' ></AmcatGroup>"><font color ="red">AIA: Test Call</font></button>
    </p>
    <p>
        <img id="btnBack" onclick="history.go(-1)" alt="Back" height="40" src="../BloomBase/Shared/images/backjpg.jpg" width="85" />
    </p>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="customerID" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="PKCustomerRecord" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="setPageName" value="" type="hidden" />
    <input id="setscripttype" value="OB" type="hidden" />
    <input id="ScriptPage" maxlength="50" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="ScriptPage" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="Call_List" type="hidden" maxlength="50" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="Call_List" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
