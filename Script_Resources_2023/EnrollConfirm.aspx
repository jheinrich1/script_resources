﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="EnrollConfirm.aspx.cs" Inherits="Script_Resources_2023.EnrollConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
    Enrollment
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
    Enrollment Confirmation
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <span style="display: none">
        <input id="tbxGUID" type="text" runat="server" />
        <input id="En1ID" style="width: auto" type="text" runat="server" />
        <input id="En2ID" style="width: auto" type="text" runat="server" />
        <input id="En3ID" style="width: auto" type="text" runat="server" />
        <input id="En4ID" style="width: auto" type="text" runat="server" />
        <input id="SpouseID" style="width: auto" type="text" runat="server" />
    </span>

    <!-- BEGIN Single Enrollment One Product -->
    <div id="DivOneApp" class="container" style="width: 100%; display: block">
        <div class="row Hide">
            <input id="CurrentAgentLogin2" type="hidden" runat="server" amcatgroupattributes='<AmcatGroup type="HiddenField" Fieldname="CurrentAgentLogin"></AmcatGroup>' />
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Plan Type:</div>
            <div class="col-6">
                <select id="PlanTypeMAPDP" runat="server" class="form-control">
                    <option value=""></option>
                    <option value="MA">MA</option>
                    <option value="PDP">PDP</option>
                </select>
            </div>
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Quoted Premium:</div>
            <div class="col-6">
                <input id="PremQuote" type="text" name="PremQuote" runat="server" class="form-control" />
            </div>
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Enrollment ID:</div>
            <div class="col-6">
                <input id="Enrollment_ID" class="required form-control" onfocus="checkCurrency(document.getElementById('Content_PremQuote').value)" name="Enrollment_ID" runat="server" />
            </div>       
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Plan Name:</div>
            <div class="col-6">
                <asp:DropDownList runat="server" ID="EnrollmentPlanName" CssClass="form-control"></asp:DropDownList>
            </div>          
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Verification ID:</div>
            <div class="col-6">
                <input id="VerificationID" type="text" maxlength="20" runat="server" class="form-control" />
            </div>        
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">HICN:</div>
            <div class="col-6">
                <input id="HICN" type="text" maxlength="20" runat="server" class="form-control" />
            </div>          
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Plan ID:</div>
            <div class="col-6">
                <input id="PlanID" type="text" maxlength="20" runat="server" class="form-control" />
            </div>           
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-3">Plan Effective Date:</div>
            <div class="col-6 d-inline-flex">
                <input id="EffDate" class="enrollment_datepicker form-control" style="background-color: #FFFFFF;" type="text" readonly="readonly" maxlength="10" runat="server" />
            </div>
        </div>
        <div class="row align-items-center mb-3">
            <div class="col-3">Product:</div>
            <div class="col-6">
                <asp:DropDownList runat="server" ID="Product" class="form-control">
                    <asp:ListItem Value="" Selected="True"> </asp:ListItem>
                    <asp:ListItem Value="MAPD">MAPD</asp:ListItem>
                    <asp:ListItem Value="PDP">PDP</asp:ListItem>
                </asp:DropDownList>
            </div>     
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-9">
                <input id="checkConfirmationNum" class="form-control btn btn-secondary" onmousedown="CheckEffDate('EffDate'); checkPlanID_Enrollment('Content_PlanID')" onclick="checkconfirmation('Advise'); confirmationButtonClicked_Enrollment();" type="button" value="Click When Confirmation Added" name="checkConfirmationNum" runat="server" />
            </div>
        </div>
        <div class="row align-items-center mb-1">
            <div class="col-9">
                <p id="LabelAlerts" class="agentComment Red Bold"></p>
            </div>
        </div>
        <div class="row Hide">
            <textarea id="EnrollmentURI" style="display: none" name="EnrollmentURI" rows="10" readonly="readonly" cols="100" runat="server" />
            <input id="CurrentAgentLogin" type="hidden" runat="server" amcatgroupattributes='<AmcatGroup type="HiddenField" Fieldname="CurrentAgentLogin"></AmcatGroup>' />
        </div>
    </div>
    <!-- END Single Enrollment One Product -->

    <div class="container">
        <div class="row align-items-center mb-3">
            <div class="col-9 d-inline-flex">
                <em><strong>Check Here to enter information for a 2-Product Enrollment Only:</strong></em>
                <input id="dual" style="margin-left: 5px;" onclick="checkdualdouble()" type="checkbox" name="dual" runat="server" />
            </div>
        </div>
        <div class="row align-items-center mb-3">
            <div class="col-9 d-inline-flex">
                <em><strong>Check Here to enter information for a Double Enrollment Only:</strong></em>
                <input id="chvdouble" style="margin-left: 5px;" onclick="checkdualdouble(); document.getElementById('sameAddress').checked = true; checksameAddress();" onmouseover="isConfirmationNumClicked_Enrollment()" type="checkbox" name="chvdouble" runat="server" />
            </div>
        </div>
    </div>

    <!-- BEGIN Single Enrollment Two Product -->
    <div id="DivDual" style="width: 100%">
        <div class="container" style="width: 100%; display: block;">
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <strong><u><span>2-PRODUCT ENROLLMENT:</span></u></strong>
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Quoted Premium:</div>
                <div class="col-6">
                    <input id="PremQuote2" class="form-control" type="text" name="PremQuote2" runat="server" />
                </div>          
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Enrollment ID:</div>
                <div class="col-6">
                    <input id="Enrollment_ID2" class="form-control" onfocus="checkCurrency(document.getElementById('Content_PremQuote2').value)" name="Enrollment_ID2" runat="server" />
                </div>
                
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Plan Name:</div>
                <div class="col-6">
                    <asp:DropDownList runat="server" ID="EnrollmentPlanName2" CssClass="form-control"></asp:DropDownList>
                </div>
                
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-3">Verification ID:</div>
                <div class="col-6">
                    <input id="VerificationID2" type="text" maxlength="20" runat="server" class="form-control"/>
                </div>
                
            </div>
            <div class="row Hide">
                <textarea id="EnrollmentURI2" style="display: none" name="EnrollmentURI2" rows="3" readonly="readonly" cols="500" runat="server" />
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <input id="checkConfirmationNum2" class="form-control btn btn-secondary" onclick="checkconfirmation2('Advise')" type="button" value="Click When Confirmation Added" name="checkConfirmationNum2" runat="server" />
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <p id="LabelAlerts2" class="agentComment Red Bold"></p>
                </div>
            </div>
        </div>
    </div>
    <!-- END Single Enrollment Two Product -->

    <!-- BEGIN Double Enrollment One Product -->
    <div id="DivDouble">
        <div id="doubletable" class="container">
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <strong><u><span>DOUBLE ENROLLMENT:</span></u></strong>
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-2">First Name:</div>
                <div class="col-3">
                    <input id="fname" class="form-control" type="text" name="fname" runat="server" />
                </div>
                <div class="col-2">Last Name:</div>
                <div class="col-3">
                    <input id="lname" class="form-control" type="text" name="lname" runat="server" />
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-2">Address:</div>
                <div class="col-5">
                    <input id="address" class="form-control" type="text" name="address" runat="server" />
                </div>
                <div class="col-3">
                    <input id="sameAddress" onclick="checksameAddress()" type="checkbox" name="sameAddress" />
                    <strong>Same Address As Applicant</strong>
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-2">City:</div>
                <div class="col-3">
                    <input id="city2" class="form-control" type="text" name="city" runat="server" />
                </div>
                <div class="col-2">State:</div>
                <div class="col-3">
                    <select id="state2" class="form-control" size="1" name="state2" runat="server">
                        <option value="" selected="selected"></option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DC">DC</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-2">Zip:</div>
                <div class="col-3">
                    <input id="zip2" class="form-control" type="text" name="zip2" runat="server" />
                </div>
                <div class="col-2">County:</div>
                <div class="col-3">
                    <input id="county2" class="form-control" type="text" name="county2" runat="server" />
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Phone Number:</div>
                <div class="col-6">
                    <input id="phoneNumber" class="form-control" type="text" name="phoneNumber" runat="server" />
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Quoted Premium:</div>
                <div class="col-6">
                    <input id="PremQuote3" class="form-control" type="text" name="PremQuote3" runat="server" />
                </div>        
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Enrollment ID:</div>
                <div class="col-6">
                    <input id="Enrollment_ID3" class="form-control" onfocus="checkCurrency(document.getElementById('Content_PremQuote3').value)" name="Enrollment_ID3" runat="server" />
                </div>               
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Plan Name:</div>
                <div class="col-6">
                    <asp:DropDownList runat="server" CssClass="form-control" ID="EnrollmentPlanName3"></asp:DropDownList>
                </div>              
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Verification ID:</div>
                <div class="col-6">
                    <input id="VerificationID3" class="form-control" type="text" maxlength="20" runat="server" />
                </div>             
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">HICN:</div>
                <div class="col-6">
                    <input id="HICN3" class="form-control" type="text" maxlength="20" runat="server" />
                </div>          
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Plan ID:</div>
                <div class="col-6">
                    <input id="PlanID3" class="form-control" type="text" maxlength="20" runat="server" />
                </div>            
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Plan Effective Date:</div>
                <div class="col-6 d-inline-flex">
                    <input id="EffDate3" class="enrollment_datepicker form-control" style="background-color: #FFFFFF;" type="text" readonly="readonly" maxlength="10" runat="server" />
                </div>            
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-3">Product:</div>
                <div class="col-6">
                    <asp:DropDownList runat="server" ID="Product3" CssClass="form-control">
                        <asp:ListItem Value="" Selected="True"> </asp:ListItem>
                        <asp:ListItem Value="MAPD">MAPD</asp:ListItem>
                        <asp:ListItem Value="PDP">PDP</asp:ListItem>
                        <asp:ListItem Value="OSB">OSB</asp:ListItem>
                    </asp:DropDownList>
                </div>                
            </div>
            <div class="row Hide">
                <textarea id="EnrollmentURI3" style="display: none" name="EnrollmentURI3" rows="3" readonly="readonly" cols="500" runat="server" />
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <input id="checkConfirmationNum3" class="form-control btn btn-secondary" onfocus="checkDoubleFields();" onmousedown="CheckEffDate('EffDate3'); checkPlanID_Enrollment('Content_PlanID3')" onclick="checkconfirmation3('Advise')" type="button" value="Click When Confirmation Added" name="checkConfirmationNum3" runat="server" />
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <p id="LabelAlerts3" class="agentComment Red Bold"></p>
                </div>
            </div>
        </div>
    </div>
    <!-- END Double Enrollment One Product -->

    <div id="doublecheckBoxDiv" class="Hide">
        <div class="container">
            <div class="row align-items-center mb-3">
                <div class="col-9 d-inline-flex">
                    <em><strong>Check Here to enter information for a 2-Product Enrollment Only for Double:</strong></em>
                    <input id="dualDouble" style="margin-left:5px;" onclick="checkdualdouble()" type="checkbox" name="dualDouble" runat="server" />
                </div>
            </div>
        </div>
    </div>
    
    <!-- BEGIN Double Enrollment Two Product -->
    <div id="dualdoubletable">
        <div class="container">
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <strong><u><span>2-PRODUCT DOUBLE ENROLLMENT:</span></u></strong>
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Quoted Premium:</div>
                <div class="col-6">
                    <input id="PremQuote4" class="form-control" type="text" name="PremQuote4" runat="server" />
                </div>               
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Enrollment ID:</div>
                <div class="col-6">
                    <input id="Enrollment_ID4" class="form-control" onfocus="checkCurrency(document.getElementById('Content_PremQuote4').value)" name="Enrollment_ID4" runat="server" />
                </div>
                
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-3">Plan Name:</div>
                <div class="col-6">
                    <asp:DropDownList CssClass="form-control" runat="server" ID="EnrollmentPlanName4"></asp:DropDownList>
                </div>               
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-3">Verification ID:</div>
                <div class="col-6">
                    <input id="VerificationID4" class="form-control" type="text" maxlength="20" runat="server" />
                </div>               
            </div>
            <div class="row Hide">
                <textarea id="EnrollmentURI4" style="display: none" name="EnrollmentURI4" rows="3" readonly="readonly" cols="500" runat="server" />
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <input id="checkConfirmationNum4" class="form-control btn btn-secondary" onclick="checkconfirmation4('Advise')" type="button" value="Click When Confirmation Added" name="checkConfirmationNum4" runat="server" />
                </div>
            </div>
            <div class="row align-items-center mb-1">
                <div class="col-9">
                    <p id="LabelAlerts4" class="agentComment Red Bold"></p>
                </div>
            </div>
        </div>
    </div>
    <!-- END Double Enrollment Two Product -->

    <p>
        <img id="btnBack" onclick="history.go(-1)" style="vertical-align: inherit" alt="Back" height="40" src="../../BloomBase/Shared/images/backjpg.jpg" width="85" />
        &nbsp;
        <asp:ImageButton ID="nextAsp" OnClick="CheckAllAndDoCommits" Width="85" Height="40"
            runat="server" BorderWidth="0" AlternateText="next" ImageUrl="../../BloomBase/Shared/images/Next.jpg"
            OnClientClick="javascript: if (document.getElementById('Content_tbxGUID').value.length > 0){window.external.SetCallInfo('BloomEnrollmentGUID', document.getElementById('Content_tbxGUID').value);}" />
    </p>
   
    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
    <input id="ContactNumber" type="hidden" maxlength="50" runat="server" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactPhoneNumbers" FieldName="ContactNumber" StrongValidation="True" AllowNulls="False" ValidationType="us-phone"></AmcatGroup>' />

    <script>
        var isCheckConfirmationNumBtnClicked = false;
        $(document).ready(function () {
            if (document.getElementById("Content_chvdouble").checked == true) {
                isCheckConfirmationNumBtnClicked = true;
            } else {
                isCheckConfirmationNumBtnClicked = false;
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" type="hidden" />
    <input id="setscripttype" value="Enroll" type="hidden" />
    <input id="AddEnrollLoadEvents" value="yes" type="hidden" />
    <input id="firstname" type="hidden" maxlength="50" runat="server" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="firstname" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="lastname" type="hidden" maxlength="50" runat="server" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="lastname" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="Address1" style="width: 384px; height: 22px" readonly type="hidden" maxlength="60" size="51" runat="server" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="Address1" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="city" type="hidden" maxlength="50" runat="server" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="city" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="county" type="hidden" maxlength="50" runat="server" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="county" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="zip" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="zip" StrongValidation="False" AllowNulls="True" ValidationType="us-zip"></AmcatGroup>' />
    <input id="state" type="hidden" amcatgroupattributes='<AmcatGroup Type="HiddenField" TableName="ContactInfo" FieldName="state"></AmcatGroup>' />
    <input id="CustomerRecord" type="hidden" name="CustomerRecord" runat="server" />
</asp:Content>
