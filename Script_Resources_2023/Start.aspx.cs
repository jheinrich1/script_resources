﻿using BloomBase.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;

namespace Script_Resources_2023
{
    public partial class Start : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string currentPageFileName = new FileInfo(Request.Url.LocalPath).Name;
                RenderMenus(MenuLinkHolder.GetMenuLinks(Convert.ToInt32(ConfigurationManager.AppSettings["scriptID"]), currentPageFileName));
            }
        }

        protected void RenderMenus(List<MenuLinkHolder> MenuList)
        {

            foreach (var item in MenuList)
            {
                if (item.ISDeleted.Equals(false) || item.ISDeleted.Equals("false"))
                {
                    //Checking if ExternalLink flag is true
                    if (item.ExternalLink.Equals(true))
                    {
                        //URL
                        if ((!string.IsNullOrEmpty(item.URL)) && (string.IsNullOrEmpty(item.FuncName) || string.IsNullOrWhiteSpace(item.FuncName)))
                        {
                            links.InnerHtml += $"<li class='nav-item'><a class='nav-link' href=microsoft-edge:http://{item.URL}>{item.MenuName}</a></li>";
                        }
                        //Function Name
                        else if ((!string.IsNullOrEmpty(item.FuncName)) && string.IsNullOrEmpty(item.URL) || string.IsNullOrWhiteSpace(item.URL))
                        {
                            links.InnerHtml += $"<li class='nav-item'><a class='nav-link' onclick={item.FuncName}>{item.MenuName}</a></li>";
                        }
                        //Both
                        else if ((!string.IsNullOrEmpty(item.URL)) && (!string.IsNullOrEmpty(item.URL)))
                        {
                            links.InnerHtml += $"<li class='nav-item'><a class='nav-link' href=http://{item.URL} onclick={item.FuncName}>{item.MenuName}</a></li>";
                        }
                    }
                    else
                    {
                        //URL
                        if ((!string.IsNullOrEmpty(item.URL)) && (string.IsNullOrEmpty(item.FuncName) || string.IsNullOrWhiteSpace(item.FuncName)))
                        {
                            links.InnerHtml += $"<li class='nav-item'><a class='nav-link' href={item.URL}>{item.MenuName}</a></li>";
                        }
                        //Function Name
                        else if ((!string.IsNullOrEmpty(item.FuncName)) && string.IsNullOrEmpty(item.URL) || string.IsNullOrWhiteSpace(item.URL))
                        {
                            links.InnerHtml += $"<li class='nav-item'><a class='nav-link' onclick={item.FuncName}>{item.MenuName}</a></li>";
                        }
                        //Both
                        else if ((!string.IsNullOrEmpty(item.URL)) && (!string.IsNullOrEmpty(item.URL)))
                        {
                            links.InnerHtml += $"<li class='nav-item'><a class='nav-link' href={item.URL} onclick={item.FuncName}>{item.MenuName}</a></li>";
                        }
                    }
                }
            }
        }
    }
}