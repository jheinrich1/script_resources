﻿using BloomBase.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Script_Resources_2023
{
    public partial class EnrollConfirm : System.Web.UI.Page
    {
        EnrollmentSaverMig es1;
        EnrollmentSaverMig es2;
        EnrollmentSaverMig es3;
        EnrollmentSaverMig es4;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AddEnrollmentPlans();
            }
            CheckCustomerRecord();
            CheckGUID();
            CheckEnrollmentID("En1");
            CheckEnrollmentID("En2");
            CheckEnrollmentID("En3");
            CheckEnrollmentID("En4");
            CheckEnrollmentID("SpouseID");
        }

        public void CheckCustomerRecord()
        {
            try
            {
                CustomerRecord.Value = Request.QueryString["CustomerRecord"];
            }
            catch
            {
                CustomerRecord.Value = "1";
            }
        }

        public void CheckGUID()
        {
            if (tbxGUID.Value == "")
            {
                if (Request.Cookies.Get("GUID") != null)
                {
                    tbxGUID.Value = Request.Cookies.Get("GUID").Value;
                }
                else
                {
                    tbxGUID.Value = Guid.NewGuid().ToString();
                }
            }
        }
        protected void CheckEnrollmentID(string enrollmentNumber)
        {
            if (Request.Cookies.Get(enrollmentNumber) != null)
            {
                if (enrollmentNumber == "En1")
                    En1ID.Value = Request.Cookies.Get(enrollmentNumber).Value;
                if (enrollmentNumber == "En2")
                    En2ID.Value = Request.Cookies.Get(enrollmentNumber).Value;
                if (enrollmentNumber == "En3")
                    En3ID.Value = Request.Cookies.Get(enrollmentNumber).Value;
                if (enrollmentNumber == "En4")
                    En4ID.Value = Request.Cookies.Get(enrollmentNumber).Value;
                if (enrollmentNumber == "SpouseID")
                    SpouseID.Value = Request.Cookies.Get(enrollmentNumber).Value;
            }
        }

        public void AddEnrollmentPlans()
        {
            try
            {
                var secondemptyItem = new ListItem
                {
                    Text = "",
                    Value = ""
                };

                var planList = EnrollmentPlan.GetPlans(ConfigurationManager.AppSettings["scriptID"]);
                planList.Sort((a, b) => a.PlanName.CompareTo(b.PlanName));

                if (planList.Count() > 0)
                {
                    if (!string.IsNullOrEmpty(EnrollmentPlan.Error))
                    {
                        lblError.Text = EnrollmentPlan.Error;
                        return;
                    }
                    EnrollmentPlanName.Items.Add(secondemptyItem);
                    EnrollmentPlanName2.Items.Add(secondemptyItem);
                    EnrollmentPlanName3.Items.Add(secondemptyItem);
                    EnrollmentPlanName4.Items.Add(secondemptyItem);
                    foreach (var k in planList)
                    {
                        var item2 = new ListItem
                        {
                            Value = k.PKPlan.ToString(),
                            Text = k.PlanName
                        };
                        EnrollmentPlanName.Items.Add(item2);
                        EnrollmentPlanName2.Items.Add(item2);
                        EnrollmentPlanName3.Items.Add(item2);
                        EnrollmentPlanName4.Items.Add(item2);
                    }
                }
                else
                {
                    lblError.Text = "No carrier groups have been assigned to this script. Please assign a carrier group via BCCM.";

                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        protected void createDoubleRecord()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DoubleEnroll"].ConnectionString);
            SqlCommand cmd = new SqlCommand("usp_Bloom_CreateSpouseRecord", conn)
            {
                CommandType = CommandType.StoredProcedure
            };

            string temp = "firstname='" + fname.Value +
                "',lastname='" + lname.Value +
                "',Address1='" + address.Value +
                "',city='" + city2.Value +
                "',state='" + state2.Value +
                "',zip='" + zip2.Value +
                "',county='" + county2.Value + "'";

            cmd.Parameters.Add(new SqlParameter("@contactNumber", SqlDbType.NVarChar)).Value = phoneNumber.Value;
            cmd.Parameters.Add(new SqlParameter("@UpdateString", SqlDbType.NVarChar)).Value = temp;

            SqlParameter pkCustomer = cmd.Parameters.Add("@PKCustomerRecord", SqlDbType.Int);
            pkCustomer.Direction = ParameterDirection.Output;

            SqlParameter pkSystem = cmd.Parameters.Add("@PKSystemRecord", SqlDbType.Int);
            pkSystem.Direction = ParameterDirection.Output;

            SqlParameter pkContact = cmd.Parameters.Add("@PKContact", SqlDbType.Int);
            pkContact.Direction = ParameterDirection.Output;

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();

                CustomerRecord.Value = cmd.Parameters["@PKCustomerRecord"].Value.ToString();
            }
            catch (Exception e)
            {
                JSAlert("Issue saving spouse: " + e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        protected bool DoubleRecordIsPopulated()
        {
            if (fname.Value == "" ||
                lname.Value == "" ||
                address.Value == "" ||
                city2.Value == "" ||
                state2.Value == "" ||
                zip2.Value == "" ||
                county2.Value == "" ||
                phoneNumber.Value == "")
            {
                return false;
            }

            return true;
        }

        protected bool IsValidId(string planId)
        {
            if (planId.Length == 9 && planId.ElementAt(5) == '-')
            {
                return true;
            }
            JSAlert("Not a Valid Plan ID. It should be in the format 'XXXXX-XXX'.");
            return false;
        }

        //Ensures enrollment is populated.
        //If yes, it creates an EnrollmentSaverMig class object containg all the enrollment data.
        protected bool EnIsPopulated(
            ref EnrollmentSaverMig enrollObject,
            string guid,
            string enrollmentID,
            string verificationID,
            string enrollmentURI,
            string customerRecord,
            string enrollPlanName,
            string quote,
            string HICN = null,
            string planID = null,
            string effDate = null,
            string product = null
            )
        {
            if (guid == "" ||
                enrollmentID == "" ||
                enrollmentURI == "" ||
                customerRecord == "" ||
                enrollPlanName == "" ||
                quote == "" ||
                HICN == "" ||
                planID == "" ||
                !IsValidId(planID) ||
                effDate == "" ||
                product == ""
                )
            {
                return false;
            }
            else
            {
                enrollObject = new EnrollmentSaverMig(guid, enrollmentID, verificationID, enrollmentURI, Convert.ToInt32(customerRecord), Convert.ToInt32(enrollPlanName), planID, HICN, effDate, product); //added planID
                enrollObject.Quote = quote;
                return true;
            }
        }

       

        protected void SetSpouseDispo(int spouseID)
        {
            SqlCommand command = null;
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["DoubleEnroll"].ConnectionString);

            try
            {
                command = new SqlCommand("usp_Bloom_DispositionDoubleEnrollment", sqlConn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add("@PKCustomerRecord", SqlDbType.Int).Value = spouseID;
                sqlConn.Open();

                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                JSAlert(e.Message.ToString());
            }
            finally
            {
                sqlConn.Close();
            }
        }
        public void JSAlert(string alertText)
        {
            var message = new JavaScriptSerializer().Serialize(alertText);
            var script = string.Format("alert({0});", message);
            ClientScript.RegisterClientScriptBlock(GetType(), "Alert", script, true);
        }
        protected void PageRedirect()
        {
            Response.Redirect("EnrollDispos.aspx?Enrollment1=" + Enrollment_ID.Value + "&Enrollment2=" + Enrollment_ID2.Value +
                              "&Enrollment3=" + Enrollment_ID3.Value + "&Enrollment4=" + Enrollment_ID4.Value + "&PlanName1=" +
                              EnrollmentPlanName.SelectedItem.Text.ToString() + "&PlanName2=" + EnrollmentPlanName2.SelectedItem.Text.ToString() +
                              "&Enrollment3=" + EnrollmentPlanName3.SelectedItem.Text.ToString() + "&Enrollment4=" +
                              EnrollmentPlanName4.SelectedItem.Text.ToString() + "&PlanID=" + PlanID.Value.ToString() +
                              "&Product=" + Product.SelectedItem.Value.ToString());
        }
        protected bool CheckSpouseFields()
        {
            if (fname.Value == "" ||
                lname.Value == "" ||
                address.Value == "" ||
                city2.Value == "" ||
                state2.Value == "" ||
                zip2.Value == "" ||
                phoneNumber.Value == "")
            {
                return false;
            }
            return true;
        }

        protected bool CheckEnroll1()
        {
            if (EnIsPopulated(ref es1, tbxGUID.Value, Enrollment_ID.Value, VerificationID.Value, EnrollmentURI.Value, CustomerRecord.Value, 
                EnrollmentPlanName.SelectedItem.Value.ToString(), PremQuote.Value, HICN.Value, PlanID.Value, EffDate.Value, Product.SelectedItem.Value) == false)
            {
                return false;
            }
            return true;
        }

        protected bool CheckEnroll2()
        {
            if (EnIsPopulated(ref es2, tbxGUID.Value, Enrollment_ID2.Value, VerificationID2.Value, EnrollmentURI2.Value, CustomerRecord.Value, 
                EnrollmentPlanName2.SelectedItem.Value.ToString(), PremQuote2.Value, HICN.Value, PlanID.Value, EffDate.Value, Product.SelectedItem.Value) == false)
            {
                return false;
            }
            return true;
        }

        protected bool CheckEnroll3()
        {
            if (EnIsPopulated(ref es3, tbxGUID.Value, Enrollment_ID3.Value, VerificationID3.Value, EnrollmentURI3.Value, CustomerRecord.Value, 
                EnrollmentPlanName3.SelectedItem.Value.ToString(), PremQuote3.Value, HICN3.Value, PlanID3.Value, EffDate3.Value, Product3.SelectedItem.Value) == false)
            {
                return false;
            }
            return true;
        }

        protected bool CheckEnroll4()
        {
            if (EnIsPopulated(ref es4, tbxGUID.Value, Enrollment_ID4.Value, VerificationID4.Value, EnrollmentURI4.Value, CustomerRecord.Value, 
                EnrollmentPlanName4.SelectedItem.Value.ToString(), PremQuote4.Value, HICN3.Value, PlanID3.Value, EffDate3.Value, Product3.SelectedItem.Value) == false)
            {
                return false;
            }
            return true;
        }

        protected bool CheckEnrollmentFields()
        {
            if (dual.Checked == false && chvdouble.Checked == false && dualDouble.Checked == false)
            {
                if (CheckEnroll1() == false)
                    return false;
            }

            if (dual.Checked == true && chvdouble.Checked == false && dualDouble.Checked == false)
            {
                if (CheckEnroll1() == false || CheckEnroll2() == false)
                    return false;
            }

            if (dual.Checked == false && chvdouble.Checked == true && dualDouble.Checked == false)
            {
                if (CheckEnroll1() == false || CheckEnroll3() == false)
                    return false;
            }

            if (dual.Checked == true && chvdouble.Checked == true && dualDouble.Checked == false)
            {
                if (CheckEnroll1() == false || CheckEnroll2() == false || CheckEnroll3() == false)
                    return false;
            }

            if (dual.Checked == false && chvdouble.Checked == true && dualDouble.Checked == true)
            {
                if (CheckEnroll1() == false || CheckEnroll3() == false || CheckEnroll4() == false)
                    return false;
            }

            if (dual.Checked == true && chvdouble.Checked == true && dualDouble.Checked == true)
            {
                if (CheckEnroll1() == false || CheckEnroll2() == false || CheckEnroll3() == false || CheckEnroll4() == false)
                    return false;
            }

            return true;
        }

        public void UpsertEnroll(string enrollmentID, string enrollmentNumber, ref EnrollmentSaverMig enrollmentObject)
        {
            if (enrollmentID == "")
            {
                if (Request.Cookies.Get(enrollmentNumber) == null)
                {
                    CreateCookie(enrollmentNumber, enrollmentObject.UpSertEnrollment().ToString());
                }
                else
                {
                    enrollmentObject.UpSertEnrollment(Convert.ToInt32(Request.Cookies.Get(enrollmentNumber).Value));
                }
            } else
            {
                enrollmentObject.UpSertEnrollment(Convert.ToInt32(enrollmentID));
            }
        }

        protected void SingleEnrollmentOneProduct()
        {
            if (dual.Checked == false && chvdouble.Checked == false && dualDouble.Checked == false)
            {
                UpsertEnroll(En1ID.Value.ToString(), "En1", ref es1);
            }
        }

        protected void SingleEnrollmentTwoProduct()
        {
            if (dual.Checked == true && chvdouble.Checked == false && dualDouble.Checked == false)
            {
                UpsertEnroll(En1ID.Value.ToString(), "En1", ref es1);
                UpsertEnroll(En2ID.Value.ToString(), "En2", ref es2);
            }
        }

        protected void SingleEnrollmentOneProduct_DoubleEnrollmentOneProduct()
        {
            if (dual.Checked == false && chvdouble.Checked == true && dualDouble.Checked == false)
            {
                UpsertEnroll(En1ID.Value.ToString(), "En1", ref es1);
                UpsertEnroll(En3ID.Value.ToString(), "En3", ref es3);
            }
        }

        protected void SingleEnrollmentTwoProduct_DoubleEnrollmentOneProduct()
        {
            if (dual.Checked == true && chvdouble.Checked == true && dualDouble.Checked == false)
            {
                UpsertEnroll(En1ID.Value.ToString(), "En1", ref es1);
                UpsertEnroll(En2ID.Value.ToString(), "En2", ref es2);
                UpsertEnroll(En3ID.Value.ToString(), "En3", ref es3);
            }
        }

        protected void SingleEnrollmentOneProduct_DoubleEnrollmentTwoProduct()
        {
            if (dual.Checked == false && chvdouble.Checked == true && dualDouble.Checked == true)
            {
                UpsertEnroll(En1ID.Value.ToString(), "En1", ref es1);
                UpsertEnroll(En3ID.Value.ToString(), "En3", ref es3);
                UpsertEnroll(En4ID.Value.ToString(), "En4", ref es4);
            }
        }

        protected void SingleEnrollmentTwoProduct_DoubleEnrollmentTwoProduct()
        {
            if (dual.Checked == true && chvdouble.Checked == true && dualDouble.Checked == true)
            {
                UpsertEnroll(En1ID.Value.ToString(), "En1", ref es1);
                UpsertEnroll(En2ID.Value.ToString(), "En2", ref es2);
                UpsertEnroll(En3ID.Value.ToString(), "En3", ref es3);
                UpsertEnroll(En4ID.Value.ToString(), "En4", ref es4);
            }
        }


        protected void WriteEnrollmentsToDB()
        {
            if (CheckEnrollmentFields())
            {
                SingleEnrollmentOneProduct();
                SingleEnrollmentTwoProduct();
                SingleEnrollmentOneProduct_DoubleEnrollmentOneProduct();
                SingleEnrollmentTwoProduct_DoubleEnrollmentOneProduct();
                SingleEnrollmentOneProduct_DoubleEnrollmentTwoProduct();
                SingleEnrollmentTwoProduct_DoubleEnrollmentTwoProduct();

                CreateCookie("GUID", tbxGUID.Value);
            }
            else
            {
                JSAlert("There is missing or invalid info on this page.\n" +
                        "Please fill in all blanks and create all pdf's needed.\n" +
                        "Also Make sure that the EnrollmentID is valid");
            }

        }
        protected void CreateCookie(string cookiename, string cookievalue)
        {
            HttpCookie cookie = new HttpCookie(cookiename)
            {
                // set the cookies value
                Value = cookievalue
            };

            // set the cookie to expire in 1 minute
            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(0, 0, 40, 0);
            cookie.Expires = dtNow + tsMinute;
            // add the cookie
            Response.Cookies.Add(cookie);
        }

        protected void CheckAllAndDoCommits(object sender, EventArgs evarg)
        {
            if (chvdouble.Checked) //Double enrollment checkbox
            {
                if (CustomerRecord.Value != "")
                {
                    WriteEnrollmentsToDB();
                    SetSpouseDispo(Convert.ToInt32(CustomerRecord.Value));
                    if (CheckEnrollmentFields())
                    {
                        PageRedirect();
                    }
                }
                else
                {
                    if (CheckSpouseFields())
                    {
                        createDoubleRecord();
                        if (CustomerRecord.Value != "")
                        {
                            WriteEnrollmentsToDB();
                            SetSpouseDispo(Convert.ToInt32(CustomerRecord.Value));
                            if (CheckEnrollmentFields())
                            {
                                PageRedirect();
                            }
                        }
                    }
                    else
                    {
                        JSAlert("Missing spouse info.");
                    }
                }
            }
            else
            {
                WriteEnrollmentsToDB();
                if (CheckEnrollmentFields())
                {
                    PageRedirect();
                }
            }
        }
    }
}