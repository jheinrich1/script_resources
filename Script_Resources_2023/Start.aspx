﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Start.aspx.cs" Inherits="Script_Resources_2023.Start" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
    Inbound - Call
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
    <li id="links" runat="server"></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
    Start
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <p>
        <img id="closeBtn" onclick="return goToEnrollConfirm()" alt="Next" height="40" src="../BloomBase/Shared/images/Next.jpg" width="85" />
    </p>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="setscripttype" value="IB" type="hidden" />
    <input id="ScriptPage" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="ScriptPage" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="Call_List" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="Call_List" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="CurrentAgentName" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" FieldName="CurrentAgentName" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="DateTimeOfCall" type="hidden" amcatgroupattributes='<AmcatGroup Type="HiddenField" TableName="ContactDetails" FieldName="DateTimeOfCall"></AmcatGroup>' />
    <input id="ContactNumber" type="hidden" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactPhoneNumbers" FieldName="ContactNumber" StrongValidation="False" AllowNulls="True"></AmcatGroup>' />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
