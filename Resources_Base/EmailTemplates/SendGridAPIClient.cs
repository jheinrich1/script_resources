﻿using System.Collections.Generic;
using SendGrid.CSharp.HTTP.Client;

namespace Resources_Base.EmailTemplates
{
    internal class SendGridAPIClient
    {
        public string Version;
        public dynamic client;

        /// <summary>
        ///     Create a client that connects to the SendGrid Web API
        /// </summary>
        /// <param name="apiKey">Your SendGrid API Key</param>
        /// <param name="baseUri">Base SendGrid API Uri</param>
        /// <param name="version"></param>
        public SendGridAPIClient(string apiKey, string baseUri = "https://api.sendgrid.com", string version = "v3")
        {
            var requestHeaders = new Dictionary<string, string>
            {
                {"Authorization", "Bearer " + apiKey},
                {"Content-Type", "application/json"},
                {"Accept", "application/json"}
            };
            client = new Client(host: baseUri, requestHeaders: requestHeaders, version: version);
        }
    }
}