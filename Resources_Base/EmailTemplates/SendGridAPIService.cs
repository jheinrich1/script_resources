﻿using Resources_Base.EmailTemplates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resources_Base.EmailTemplates
{
    public static class SendGridApiService
    {
        private const string SENDGRID_APIKEY = "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";
        public static string TEMPLATE_ID = "a9c6c91a-4e4c-4ffd-a5d9-8489fba7601c";
        public static string FROM_EMAIL = "no-reply@bloominsuranceagency.com";
        public static string FROM_NAME = "Medicare Supplement Request Team representing Aetna Life Insurance Company";
        public static List<Attachment> ATTACHMENTS = new List<Attachment>();

        private static async Task<bool> SendEmailService(IEnumerable<string> prospectEmails, Dictionary<string, string> substitutions = null, string subject = null, bool urgent = false)
        {
            dynamic sg = new SendGridAPIClient(SENDGRID_APIKEY);

            var mail = new Mail();
            var settings = new MailSettings();
            var sandboxMode = new SandboxMode { Enable = false };

            settings.SandboxMode = sandboxMode;
            mail.MailSettings = settings;
            mail.From = new Email(FROM_EMAIL, FROM_NAME);

            //if (ATTACHMENTS != null && ATTACHMENTS.Count > 0)
            //{
            //    foreach (Attachment att in ATTACHMENTS)
            //    {
            //        mail.AddAttachment(att);
            //    }
            //}

            if (urgent)
            {
                mail.Headers.Add("Priority", "Urgent");
            }

            var personalization = new Personalization();

            if (substitutions != null)
            {
                foreach (var pair in substitutions)
                {
                    personalization.AddSubstitution(pair.Key, pair.Value);
                }

                personalization.Substitutions = substitutions;
            }
            if (subject != null)
            {
                personalization.Subject = subject;
            }

            personalization.AddTos(prospectEmails.Select(x => new Email { Address = x }).ToList());

            mail.AddPersonalization(personalization);
            mail.TemplateId = TEMPLATE_ID;

            var response = await sg.client.mail.send.post(requestBody: mail.Get());

            return response.StatusCode.ToString() == "Accepted";
        }

        private static async Task<bool> SendEmailServiceWithAttachment(IEnumerable<string> prospectEmails, string attachment, Dictionary<string, string> substitutions = null, string subject = null, bool urgent = false)
        {
            dynamic sg = new SendGridAPIClient(SENDGRID_APIKEY);

            var mail = new Mail();
            var settings = new MailSettings();
            var sandboxMode = new SandboxMode { Enable = false };

            settings.SandboxMode = sandboxMode;
            mail.MailSettings = settings;
            mail.From = new Email(FROM_EMAIL, FROM_NAME);

            if (ATTACHMENTS != null && ATTACHMENTS.Count > 0)
            {
                foreach (Attachment att in ATTACHMENTS)
                {
                    mail.AddAttachment(att);
                }
            }

            if (urgent)
            {
                mail.Headers.Add("Priority", "Urgent");
            }

            var personalization = new Personalization();

            if (substitutions != null)
            {
                foreach (var pair in substitutions)
                {
                    personalization.AddSubstitution(pair.Key, pair.Value);
                }

                personalization.Substitutions = substitutions;
            }
            if (subject != null)
            {
                personalization.Subject = subject;
            }

            personalization.AddTos(prospectEmails.Select(x => new Email { Address = x }).ToList());

            mail.AddPersonalization(personalization);
            mail.TemplateId = TEMPLATE_ID;

            var response = await sg.client.mail.send.post(requestBody: mail.Get());

            return response.StatusCode.ToString() == "Accepted";
        }

        public static Task<bool> SendEmail(string emailAddress, Dictionary<string, string> substitutions = null, string subject = null, bool urgent = false)
        {
            dynamic response = SendEmailService(new List<string> { emailAddress }, substitutions, subject, urgent);
            return response;
        }

        public static Task<bool> SendEmail(List<string> emailAddresses, Dictionary<string, string> substitutions = null, string subject = null, bool urgent = false)
        {
            dynamic response = SendEmailService(emailAddresses, substitutions, subject);
            return response;
        }

        public static Task<bool> SendEmailWithAttachment(string emailAddresses, string attachment, Dictionary<string, string> substitutions = null, string subject = null, bool urgent = false)
        {
            dynamic response = SendEmailServiceWithAttachment(new List<string> { emailAddresses }, attachment, substitutions, subject, urgent);
            return response;
        }
    }
}