﻿$(document).ready(function () {
    $("#VerifyAdd").click(function () {
        window.parent.isVerifyBtnClicked = true;
        setTimeout(function () { $("#container .required").focus() }, 1000);
    });
});

function isValid(street, state, city) {
    if (document.getElementById("street").value == '' || document.getElementById("state").value == '' || document.getElementById("city").value == '') {
        return false;
    }
    else {
        return true;
    }
}

function ChooseAddress(identifier) {
    // alert("data-id:" + $(identifier).data('id') + ", data-option:" + $(identifier).data('option'));
    // alert("data-id:" + $(identifier).data('id') + ", data-option:" + $(identifier).data('option'));
    var data = $(identifier).data('option');
    var array = data.split(',');
    $("#street").val(array[0]);
    if (array[1].value == null) {
        $("#street2").val("");
    }
    else {
        $("#street2").val(array[1]);
    }

    $("#city").val(array[2]);
    $("#state").val(array[3]);
    $("#zip").val(array[4]);
    $("#plus4").val(array[5]);
    $("#county").val(array[6]);

    addressDisplay = '<div class="smarty-ui">' +
        '<a href="javascript:" class="smarty-tag smarty-tag-green">' +
        '<span class="smarty-tag-check">✓</span>' +
        ' <span class="smarty-tag-text smarty-undo">Verified</span></a>' +
        '</div>';
    document.getElementById("QASMatched").checked = true;
    document.getElementById("QASOverwroteVerification").checked = false;
    $('#VerifyAdd').addClass("Hide");
    $('#addressContainer').html(addressDisplay);

    $('#mask , .login-popup').fadeOut(300, function () {
        $('#mask').remove();
    });

}

function GoBack() {
    $('#mask , .login-popup').fadeOut(300, function () {
        $('#mask').remove();
    });
    return false;
}

function UseAsIs() {
    addressDisplay = '<div class="smarty-ui">' +
        '<a href="javascript:" class="smarty-tag smarty-tag-red">' +
        '<span class="smarty-tag-check">X</span>' +
        ' <span class="smarty-tag-text-red smarty-undo">Not Verified</span></a>' +
        '</div>';
    document.getElementById("QASMatched").checked = false;
    document.getElementById("QASOverwroteVerification").checked = true;
    $('#VerifyAdd').addClass("Hide");
    $('#addressContainer').html(addressDisplay);

    $('#mask , .login-popup').fadeOut(300, function () {
        $('#mask').remove();
    });
    return false;
}

$(document).ready(function () {
    $("#addressForm").on("reset", function (event) {
        event.preventDefault();

        $('#addressContainer').html('');
        $("#street").val('');
        $("#street2").val('');
        $("#state").val('');
        $("#city").val('');
        $("#zip").val('');
        $("#county").val('');
        $("#plus4").val('');
        $('#VerifyAdd').removeClass("Hide");
    });

    $("#addressForm").on("submit", function (event) {
        event.preventDefault();

        var valid = isValid(document.getElementById("street").value, document.getElementById("state").value, document.getElementById("city").value);
        if (!valid) {
            $('#ErrorMsg').removeClass("Hide");
            window.parent.document.getElementById("SmartyStreetsSrc").setAttribute("style", "height:592px; width: 875px; border:none;");
        }
        else {
            $('#ErrorMsg').addClass("Hide");

            var dataArray = $(this).serializeArray();
            var data = {};
            $.each(dataArray, function (index, element) {
                if (element.value) {
                    data[element.name] = element.value;
                }
            });
            var json = JSON
            $.ajax({
                url: 'http://smartystreets.rmgcom.local/api/Address',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (result) {
                    if (!result.isValid) {
                        $('#addressPopUpContainer').html('<div class="smarty-popup-header smarty-popup-invalid-header">You entered an unknown address:</div><div class="smarty-popup-typed-address">' + $("#street").val() + ' ' + $("#street2").val() + '<br>' + $("#city").val() + ' ' + $("#state").val() + ' ' + $("#zip").val() + '</div><div class="smarty-choice-alt">' +
                            '<a href="#" onclick="GoBack();" class="smarty-choice smarty-choice-abort smarty-abort">Go back</a>' +
                            '<a href="#" onclick="UseAsIs();" class="smarty-choice smarty-choice-override">Use as it is</a>' +
                            '</div>');
                        showAddressPopUp();
                        return;
                    }
                    var addressDisplay = '';
                    $.each(result.addressMatches, function (index, element) {

                        if (element.street == document.getElementById("street").value && element.city == document.getElementById("city").value && element.state == document.getElementById("state").value && element.zip == document.getElementById("zip").value) {
                            addressDisplay = '<div class="smarty-ui">' +
                                '<a href="javascript:" class="smarty-tag smarty-tag-green">' +
                                '<span class="smarty-tag-check">✓</span>' +
                                ' <span class="smarty-tag-text">Verified</span></a>' +
                                '</div>';
                            document.getElementById("QASMatched").checked = true;
                            document.getElementById("QASOverwroteVerification").checked = false;
                            $('#VerifyAdd').addClass("Hide");
                            $('#addressContainer').html(addressDisplay);
                            document.getElementById("street").value = element.street;
                            document.getElementById("street2").value = element.street2;
                            document.getElementById("state").value = element.state;
                            document.getElementById("city").value = element.city;
                            document.getElementById("zip").value = element.zip;
                            document.getElementById("county").value = element.county;
                            document.getElementById("plus4").value = element.plus4;
                        }
                        else if (element.street == document.getElementById("street").value && element.city == document.getElementById("city").value && element.state == document.getElementById("state").value) {
                            addressDisplay = '<div class="smarty-ui">' +
                                '<a href="javascript:" class="smarty-tag smarty-tag-green">' +
                                '<span class="smarty-tag-check">✓</span>' +
                                ' <span class="smarty-tag-text smarty-undo">Verified</span></a>' +
                                '</div>';
                            document.getElementById("QASMatched").checked = true;
                            document.getElementById("QASOverwroteVerification").checked = false;
                            $('#VerifyAdd').addClass("Hide");
                            $('#addressContainer').html(addressDisplay);
                            document.getElementById("street").value = element.street;
                            document.getElementById("street2").value = element.street2;
                            document.getElementById("state").value = element.state;
                            document.getElementById("city").value = element.city;
                            document.getElementById("zip").value = element.zip;
                            document.getElementById("county").value = element.county;
                            document.getElementById("plus4").value = element.plus4;
                        }
                        else {
                            if (element.street2 == null) {
                                var addresstext = element.street + ' ' + element.city + ' ' + element.state + ' ' + element.zip + '-' + element.plus4;
                            }
                            else {
                                var addresstext = element.street + ' ' + element.street2 + ' ' + element.city + ' ' + element.state + ' ' + element.zip + '-' + element.plus4;
                            }

                            addressDisplay = '<div class="smarty-popup-header smarty-popup-ambiguous-header">You entered: </div><div class="smarty-popup-typed-address">' + document.getElementById("street").value + ' ' + document.getElementById("street2").value + '<br>' + document.getElementById("city").value + ' ' + document.getElementById("state").value + ' ' + document.getElementById("zip").value + '</div>' + '<div class="smarty-popup-header smarty-popup-ambiguous-header">Did you mean one of the following? </div>' +
                                '<br><div class="smarty-choice-list">' +
                                '<a href="#" onclick="ChooseAddress(this);" class="smarty-choice" data-id="' + (index) + '" data-option="' + (element.street + ',' + element.street2 + ',' + element.city + ',' + element.state + ',' + element.zip + ',' + element.plus4 + ',' + element.county) + '">' + addresstext +
                                '</div><div class="smarty-choice-alt"><a href="#" onclick="GoBack();" class="smarty-choice smarty-choice-abort smarty-abort">Go back</a>' +
                                '<a href="#" onclick="UseAsIs();" class="smarty-choice smarty-choice-override">Use as it is</a></div>';
                            showAddressPopUp();
                            $('#addressPopUpContainer').html(addressDisplay);
                        }
                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Address Verification is unavailable. Please proceed with 'Universal Next Button' when done with page.");
                    $("#universalNextButton").show();
                    window.parent.document.getElementById("SmartyStreetsSrc").setAttribute("style", "height:598px; width: 875px; border: none;");
                },
                failure: function () {
                    $('#addressContainer').html('<h3 class="mt-3">An error occurred</h3>');
                }
                
            });
        }
    });

    // When clicking on the button close or the mask layer the popup closed
    $('body').on('click', 'a.close, #mask', function () {
        $('#mask , .login-popup').fadeOut(300, function () {
            $('#mask').remove();
        });
        return false;
    });

    function showAddressPopUp() {
        // Getting the variable's value from a link
        var loginBox = $('a.login-window').attr('href');

        //Fade in the Popup and add close button
        $(loginBox).fadeIn(300);

        //Set the center alignment padding + border
        var popMargTop = ($(loginBox).height() + 24) / 2;
        var popMargLeft = ($(loginBox).width() + 24) / 2;

        $(loginBox).css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });

        // Add the mask to body
        $('body').append('<div id="mask"></div>');
        $('#mask').fadeIn(300);

        return false;
    }

    $("#universalNextButton").click(function () {
        window.parent.location = "../../../" + window.parent.backupPage;
    })
})
