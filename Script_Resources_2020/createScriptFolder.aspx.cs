﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows;
using System.Net.Mail;
using System.Web.UI;
using System.Diagnostics;
using System.Windows.Controls;
using System.Web.UI.HtmlControls;
using Button = System.Web.UI.WebControls.Button;
using Microsoft.SqlServer.Server;
using TextBox = System.Web.UI.WebControls.TextBox;
using Panel = System.Web.UI.WebControls.Panel;
using System.Text.RegularExpressions;

namespace Script_Resources_2020
{
    public partial class createScriptFolder : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                List<TextBox> scriptNameTextboxes = new List<TextBox>();
                List<ImageButton> scriptImageButtons = new List<ImageButton>();
                List<Panel> panels = new List<Panel>();
                Session["count"] = 1;
                if (Session["scriptNameTextboxes"] == null)
                {

                    Session["scriptNameTextboxes"] = scriptNameTextboxes;
                }

                if (Session["scriptImageButtons"] == null)
                {

                    Session["scriptImageButtons"] = scriptImageButtons;
                }

                if (Session["panelList"] == null)
                {
                    Session["panelList"] = panels;
                }
                List<Panel> myPanels = Session["panelList"] as List<Panel>;
                foreach (Panel panel in Session["panelList"] as List<Panel>)
                {
                    placeholder1.Controls.Add(panel);
                }
            }
        }

        protected void CreateScript(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(scriptName.Value))
            {
                lblError.Text = "Script name cannot be empty";
                lblError.Visible = true;
                return;
            }

            var scriptname = scriptName.Value;

            //Create new scipt folder in staging scripts directory
            //System.IO.Directory.CreateDirectory("..\\Staging Scripts\\" + scriptname);
            System.IO.Directory.CreateDirectory("C:\\Staging Scripts\\" + scriptname);
            string[] lines =
            {
                "<html>", "<head>", "</head>", "<body>", "</body>", "</html>"
            };

            //Add start.htm file to newly created script folder. Populate start.htm with HTML skeleton
            //File.WriteAllLines("..\\Staging Scripts\\" + scriptname + "\\start.htm", lines);
            File.WriteAllLines("C:\\Staging Scripts\\" + scriptname + "\\start.htm", lines);


            //Add script to Scripts table in CallCenter db.
            //Script.AddScriptToDB(scriptname);
            //Add confirm prompt before creating script.
            //Create Jira ticket
            //CreateTicket(sender, e, scriptname);
        }

        public void CreateTicket(object sender, EventArgs e, string scriptName)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "docker01.rmgcom.local";
            
            MailMessage message = new MailMessage();
            message.From = new MailAddress("jheinrich@bloominsurance.com");
            message.To.Add("itsupport@bloominsurance.com");
            message.Subject = "Script to Site in IIS";
            message.Body = "Please create the following script(s) as sites in IIS UAT:\n" + scriptName;
            client.Send(message);
        }

        private TextBox CreateTextbox(string id)
        {
            TextBox tb = new TextBox();
            tb.Attributes.Add("ID", id);
            tb.Style.Add("margin-bottom", "5px");
            return tb;
        }

        private ImageButton CreateImageButton(string id)
        {
            ImageButton ib = new ImageButton();
            ib.Attributes.Add("ID", id);
            ib.ImageUrl = "icons/trash-white/white-remove-50.png";
            ib.Style.Add("margin-bottom", "5px");
            ib.Style.Add("height", "auto");
            ib.Style.Add("width", "30px");
            ib.Style.Add("margin-left", "3px");
            ib.CausesValidation = false;
            ib.Click += new ImageClickEventHandler(Delete_Click);
            return ib;
        }

        private Panel CreatePanel(string id)
        {
            Panel p = new Panel();
            p.ID = id;
            p.Style.Add("display", "inline-flex");
            return p;
        }

        public void Delete_Click(object sender, ImageClickEventArgs e)
        {
            Button clickedButton = (Button)sender;

            var id = clickedButton.ID;
            string numberOnly = Regex.Replace(id, "[^0-9.]", "");
            List<Panel> listOfPanels = (List<Panel>)Session["panelList"];
            Panel toDelete = (Panel)FindControl("panel" + numberOnly);
            listOfPanels.Remove(toDelete);
            Session["panelList"] = listOfPanels;
        }



        public void Button_Click(object sender, EventArgs e)
        {
            List<TextBox> scriptNameBoxes = (List<TextBox>)Session["scriptNameTextboxes"];
            TextBox tb = CreateTextbox("scriptName" + scriptNameBoxes.Count.ToString());
            scriptNameBoxes.Add(tb);

            List<ImageButton> imageButtons = (List<ImageButton>)Session["scriptImageButtons"];
            ImageButton ib = CreateImageButton("imageButton" + imageButtons.Count.ToString());
            imageButtons.Add(ib);
            
            List<Panel> panelList = (List<Panel>)Session["panelList"];
            Panel p = CreatePanel("panel" + panelList.Count.ToString());
            panelList.Add(p);
            

            foreach (Panel pnl in panelList)
            {           
                placeholder1.Controls.Add(pnl);
                pnl.Controls.Add(tb);
                pnl.Controls.Add(ib); 
            }
        }

        

    }
}