﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="getDispos.aspx.cs" Inherits="Script_Resources_2020.getDispos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="BannerName" runat="server">
    
</asp:content>
<asp:content id="Content3" contentplaceholderid="MenuLinkHolder" runat="server">
   <div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">Dispositions by Campaign</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="Default.aspx">Home</a>
                    <a class="nav-item nav-link" href="getFields.aspx">Amcat Fields by Client</a>
                    <a class="nav-item nav-link" href="getScriptIDs.aspx">Script ID's</a>
                    <a class="nav-item nav-link" href="getProjects.aspx">Projects & DB's by Script</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:content>
<asp:content id="Content4" contentplaceholderid="PageNameHolder" runat="server">
   
</asp:content>
<asp:content id="Content5" contentplaceholderid="Content" runat="server">
<div class="container">
    <div class="row" style="display:block">
        <p style="margin-bottom: 0px;">
          <label style="margin-bottom: 0px;">Campaign: </label>
            <ajaxToolkit:ComboBox 
                ID="listOfCampaigns" 
                runat="server"
                Width="300px"
                AutoCompleteMode="SuggestAppend" 
                OnSelectedIndexChanged="getDisposForSelectedCampaign" 
                AutoPostBack="true">
            </ajaxToolkit:ComboBox>
        </p>
    </div>

    <div class="row" id="searchFilter" runat="server" visible="false">
        <p>
            <label>Search: </label>
            <input type="text" id="filterBox" style="width: 300px;" class="form-control" />
        </p>
    </div>

    <div class="row">
        <div id="disposByCampaign" runat="server" visible="false">

        </div>
    </div>
</div>
</asp:content>
<asp:content id="Content6" contentplaceholderid="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:content>
<asp:content id="Content7" contentplaceholderid="AddressVerification" runat="server">
</asp:content>
