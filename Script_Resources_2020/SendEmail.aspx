﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="SendEmail.aspx.cs" Inherits="Script_Resources_2020.SendEmail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="BannerName" runat="server">
    
</asp:content>
<asp:content id="Content3" contentplaceholderid="MenuLinkHolder" runat="server">
   <div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">Send Email Test</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                    <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                    <a class="nav-item nav-link" href="Dbclean.aspx">Db Cleaner</a>
                    <a class="nav-item nav-link" href="DateTimePicker.aspx">Date and Time Picker</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:content>
<asp:content id="Content4" contentplaceholderid="PageNameHolder" runat="server">
   
</asp:content>
<asp:content id="Content5" contentplaceholderid="Content" runat="server">
    <p>
        <button type="button" runat="server" id="sendEmail" clientidmode="Static" onserverclick="SubmitBtn_OnClick">Send Email</button>
    </p>
    <asp:Label runat="server" ID="lblError"></asp:Label>
    <asp:Label runat="server" ID="lblSuccess"></asp:Label>
    <object data="https://image.myblue.bcbsm.com/lib/fe5d15707c630c787615/m/2/00f650c4-1761-4ea4-89d6-931463cb5fad.pdf" type="application/pdf" width="100%" height="1000px">
      <p>Unable to display PDF file. <a href="https://image.myblue.bcbsm.com/lib/fe5d15707c630c787615/m/2/00f650c4-1761-4ea4-89d6-931463cb5fad.pdf">Download</a> instead.</p>
    </object>
</asp:content>
<asp:content id="Content6" contentplaceholderid="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:content>
<asp:content id="Content7" contentplaceholderid="AddressVerification" runat="server">
</asp:content>
