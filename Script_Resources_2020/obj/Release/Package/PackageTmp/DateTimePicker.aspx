﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="DateTimePicker.aspx.cs" Inherits="Script_Resources_2020.DateTimePicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
    <div id="navigationHeader">
        <nav class="navbar navbar-expand-lg navbarHeader">
            <div class="container headerContainer">
                <a class="navbar-brand" href="#">Date and Time Picker</a>
            </div>
        </nav>
    </div>
    <div id="navigationBar">
        <nav class="navbar navbar-expand-lg">
            <div class="container navContainer">
                <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                        <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                        <a class="nav-item nav-link" href="Dbclean.aspx">Db Cleaner</a>
                        <a class="nav-item nav-link" href="js.aspx">JavaScript</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <h3>Date and Time Picker 1
                </h3>
                <p>
                    Date Picker:<br />
                    <input id="exampleDate" class="datepicker1" name="date" type="text" data-value="2014-08-08" />
                </p>
                <p>
                    Time Picker:<br />
                    <input id="exampleTime" class="timepicker" type="time" name="time" data-value="0:00" />
                </p>
            </div>
            <div class="col-4">
                <h3>Date Picker 2
                </h3>
                <p>
                    Date Picker:<br />
                    <input id="datePicker" type="text" />
                </p>
            </div>
            <div class="col-4">
                <h3>Date and Time Picker 3
                </h3>
                <p>
                    Date and Time Picker:<br />
                    <input type='text' id="dateAndTimePicker" class="datepicker-here" data-timepicker="true" data-language='en' />
                </p>
            </div>
            <div class="col-12">
                <input type="text" class="timepicker5" />
            </div>
        </div>
    </div>

    <script>
        // date picker
        $('.datepicker1').pickadate();

        // time picker
        $('.timepicker').pickatime();

        //date picker
        //$('#datePicker').dateTimePicker();

        var d = new Date();
        d = new Date();
        var date_format_str = d.getFullYear().toString() + "-" + ((d.getMonth() + 1).toString().length == 2 ? (d.getMonth() + 1).toString() : "0" + (d.getMonth() + 1).toString()) + "-" + (d.getDate().toString().length == 2 ? (d.getDate() + 1).toString() : "0" + (d.getDate() + 1).toString());

        $('#datePicker').dateTimePicker({
            limitMin: date_format_str,

            format: 'yyyy/MM/01',
        });


        $('.timepicker5').timepicker({
            timeFormat: 'h:mm p',
            interval: 60,
            minTime: '10',
            maxTime: '6:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });


    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="AddressVerification" runat="server">
</asp:Content>
