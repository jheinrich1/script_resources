﻿var dispoNames = [];
var dispoTextList = "";

//Cleans dispositions and get disposition names
function cleanMe() {
    var dirtyDispos = $("#dirtyDispositions").val();
    var lines = dirtyDispos.split('\n');
    var cleanDispositions = "";
    dispoNames = [];
    dispoTextList = "";

    let buttonstart = new RegExp(/<BUTTON ID=/g);
    let buttonend = new RegExp(/<\/BUTTON>/g);
    let fontstart = new RegExp(/<FONT COLOR=red>/g);
    let fontend = new RegExp(/<\/FONT>/g);
    let QR = new RegExp(/['"]?QR_Button_[1-9]*['"]?/);
    let RD = new RegExp(/(?<=Description=").*(?=" Result)/);


    for (var i = 0; i < lines.length; i++) {
        lines[i] = lines[i].replace(QR, "'" + lines[i].match(QR) + "'");
        lines[i] = lines[i].replace(buttonstart, '<button id=');
        lines[i] = lines[i].replace(buttonend, '</button>\n');
        lines[i] = lines[i].replace(fontstart, "<font color='red'>");
        lines[i] = lines[i].replace(fontend, '</font>');

        if (lines[i].match(RD) != null) {
            dispoNames.push(lines[i].match(RD)[0]);
        }
        cleanDispositions += lines[i] + "\n";
        dispoTextList += lines[i];
    }
    console.log("CLEAN DISPOSITIONS");
    console.log(cleanDispositions);
    console.log("DISPOSITION NAMES");
    console.log(dispoNames);
    clickCounter = 0;
    $("#cleanDispositions").html(cleanDispositions);
    $("#namesAndCategories").show();
}

//Copy Text
function copyToClipboard(element) {
    var text = $(element).clone().find('br').prepend('\r\n').end().text();
    element = $('<textarea>').appendTo('body').val(text).select();
    document.execCommand('copy');
    element.remove();
}

function copySuccess(element) {
    if ($(element).hasClass('copied')) {
        if ($(".textCopied").hasClass("Hide")) {
            $(".textCopied").removeClass("Hide");
        } else {

        }
    } else {
        $(element).addClass("copied");
        $(element).after("<span class='textCopied'>Copied text to clipboard.</span>");
        setTimeout(function () {
            $(".textCopied").addClass("Hide");
        }, 5000)
    }
};


var numOfNames = 1;
var listOfNames = [];

var numOfValues = 1;
var valueList = [];

//Create Category and Value container
function createName() {
    var nameID = "#group" + (numOfNames - 1);
    var nameVal = $(nameID).val();

    listOfNames = [];

    var nameInput =
        "<div id='dispoGroup" + numOfNames + "' class='input-group mb-3'>" +
        "<div class='input-group-prepend'>" +
        "<span class='input-group-text' id='basic-addon" + numOfNames + "'>Category " + (numOfNames + 1) + "</span>" +
        "</div >" +
        "<input type='text' class='form-control categories' id='group" + numOfNames + "'/>" +
        "</div>";

    $("#dispoNames").append(nameInput);

    listOfNames.push(nameVal);

    numOfNames += 1;
}


$(document).ready(function () {
    $("#deleteName").click(function () {
        var nameToRemove = "#dispoGroup" + (numOfNames - 1);
        $(nameToRemove).remove();
        numOfNames = numOfNames - 1;
        listOfNames.pop();
    })
});


//Create select statement and insert category and values
var listOfDispoNames = [];
function getHTMLCode() {

    listOfDispoNames = [];
    finalGroupedList = [];
    var option = "";
    var startSelect =
        "<select id='dispositions'>\n" +
        "\t<option value=''></option>\n";
    var endSelect = "</select>";



    $("input[id^='group']").each(function () {
        listOfDispoNames.push($(this).val());
    })


    for (var i = 0; i < listOfDispoNames.length; i++) {
        option = "\t<option value='" + listOfDispoNames[i] + "'>" + listOfDispoNames[i] + "</option>\n"
        startSelect += option;
    }

    startSelect += endSelect;
    document.getElementById("HTMLholder").value = startSelect;

    console.log(listOfDispoNames);
};



function getJS() {
    var JSText =
        "$(document).ready(function () {\n" +
        "\t$('#dispositions').change(function () {\n" +
        "\t\t$('.Hide').hide();\n" +
        "\t\tvar value = document.getElementById('dispositions').value;\n" +
        "\t\t$('#' + value).show();\n" +
        "\t});\n" +
        "})";

    $("#JSHolder").val(JSText);
}



var categoryCounter = 0;
var categories = "";
function createCategoryList() {


    if (nameListClicked > 0) {
        categoryCounter = 0;
        nameListClicked = 0;
    }
    $("input[id^='group']").each(function () {

        var radioInput = "<label id='label" + categoryCounter + "'>" + $(this).val() + "</label>" + "<input name='categories" + categoryCounter + "' type='radio' value='" + $(this).val() + "' />";

        categories += radioInput;
    })


    categoryCounter++;
    console.log("CategoryCounter: " + categoryCounter);
}



var nodeList = [];
var nameListClicked = 0;
//Displays names of all dispositions
function createNameList() {
    if (checkCats()) {
        var dispoCounter = 0;
        var formCounter = 0;
        var dispoID = "#dispo" + dispoCounter;
        var category = "categories" + dispoCounter;

        var labelName = "#label" + dispoCounter;

        if (nameListClicked > 0) {
            $("#dispositionNames").empty();
            dispoCounter = 0;
            formCounter = 0;
        }

        for (var i = 0; i < dispoNames.length; i++) {
            createCategoryList();
            $("#dispositionNames").append("<form class='pageForms" + formCounter + "' id='" + "form" + formCounter + "'> <div id=" + "dispo" + dispoCounter + ">" + "<p id='" + dispoNames[i] + "'>" + dispoNames[i] + "</p>" + categories + "</div></form>");
            $(labelName).attr("for", category);
            dispoCounter++;
            formCounter++;
            categories = "";
        }

        nameListClicked++;

        $("#showSelection").show();
        $("#namesAndCategories").html("Clear/Update Categories");
        $("#namesAndCategories").css
    } else {
        return false;
    }
}



var categoryList = [];
var clickCounter = 0;
function createMainCategoryList() {
    checkedCategoriesCopy = [];
    checkedCategories = [];
    var counter = 0;
    categoryList = [];


    if (clickCounter == 0) {
        dispoTextList = dispoTextList.split("\n");
        dispoTextList.pop();
    }

    for (var i = 0; i < dispoNames.length; i++) {
        var dispoList = [];
        var inputName = "input:radio[name='categories" + counter + "']:checked";
        if ($(inputName)) {
            console.log(dispoNames[i]);
            console.log($(inputName).val());
            dispoList.push(dispoNames[i]);
            dispoList.push(dispoTextList[i]);
            dispoList.push($(inputName).val());
            categoryList.push(dispoList);
            counter++;
        }
    }
    console.log(categoryList);
    clickCounter++;
    makeGroupedList();
}


var checkedCategories = [];
var checkedCategoriesCopy = []
function makeGroupedList() {

    for (var i = 0; i < categoryList.length; i++) {
        console.log(categoryList[i]);
        if (checkedCategories.includes(categoryList[i][2])) {

        }
        else {
            checkedCategories.push(categoryList[i][2]);
            checkedCategoriesCopy.push(categoryList[i][2]);
        }
    }
    console.log(checkedCategories);
    nestedList = [];
    nestedCategoryList();
}

var nestedList = []
function nestedCategoryList() {
    nestedList = [];
    for (var i = 0; i < checkedCategories.length; i++) {
        for (var y = 0; y < categoryList.length; y++) {
            if (checkedCategories[i] == categoryList[y][2]) {
                var grouping = [];
                grouping.push(checkedCategories[i]);
                grouping.push(categoryList[y][1]);
                nestedList.push(grouping);
            }
        }
    }
    console.log(nestedList);
    listOfDispoNames = [];
}



var finalGroupedList = [];
function updateContainer() {

    while (checkedCategories.length != 0) {
        var templist = [];
        var counter = 0;
        for (var i = 0; i < nestedList.length; i++) {

            if ($("input[type='checkbox']").prop("checked")) {
                if (checkedCategories[counter].includes(nestedList[i][0])) {
                    templist.push("<p>\n" + nestedList[i][1] + "\n</p>");
                }
            } else {
                if (checkedCategories[counter].includes(nestedList[i][0])) {
                    templist.push(nestedList[i][1]);
                }
            }

        }
        counter++;
        finalGroupedList.push(templist);
        checkedCategories.shift();
    }
    console.log(finalGroupedList);
    getContainer();
}


//Create content container
var containerIDList = [];

var gotContainer = false;
function getContainer() {
    var containerText = "";

    console.log(checkedCategoriesCopy);

    containerIDList = [];


    $("#HTMLcontainer").val("");

    var selectStatement = $("#HTMLholder").val();
    for (var i = 0; i < checkedCategoriesCopy.length; i++) {
        var dispoValues = finalGroupedList[i];
        console.log(finalGroupedList[i]);
        var divText = "<div id='" + checkedCategoriesCopy[i] + "' class='Hide'>\n" + dispoValues + "\n</div>\n\n";
        containerText += divText;
    }

    console.log(containerText);
    divText = "";
    newText = "";
    var newText = containerText.replace(/,/g, '\n');
    newerText = "";
    var newerText = newText.replace(/<\/button>/g, '\n<button>');
    newestText = "";
    var newestText = newerText.replace(/<font/g, '\n<font');
    console.log(newestText);
    gotContainer = true;

    $("#HTMLcontainer").val(selectStatement + "\n\n\n" + newestText);
}

function checkSelections() {

    //Get length of forms in div dispositionNames and loop that many times
    var numberOfForms = $("#dispositionNames form").length;
    var allSelectionsMade = false;

    for (var i = 0; i < numberOfForms; i++) {
        var inputName = "input[name='categories" + i + "']:checked";
        if ($(inputName).length == 0) {
            console.log(inputName + " has no selection.");
            allSelectionsMade = false;
        } else {
            allSelectionsMade = true;
        }
    }

    if (allSelectionsMade == true) {
        createMainCategoryList();
        getHTMLCode();
        updateContainer();
        getJS();
    } else {
        alert("Please select a category for each disposition.");
    }
}

function checkCats() {
    console.log($("#dispoNames div.input-group").length);
    var numOfCats = $("#dispoNames div.input-group").length;
    var inputID = "#group";
    var allClear = true;
    for (var i = 0; i < numOfCats; i++) {
        console.log(inputID + i);
        if ($(inputID + i).val() == "") {
            alert("Please provide a name for Category " + (i + 1));
            allClear = false;
            return false;
        }
    }
    console.log(allClear);
    console.log("all cats have names");
    return allClear;
}


//DB FIELD CLEANING
function cleanDB() {
    var dirtyDispos = $("#dirtyDBs").val();
    var lines = dirtyDispos.split('\n');
    var cleanDBs = "";
    DNames = [];
    dispoTextList = "";

    let inputstart = new RegExp(/<INPUT ID=/g);
    let type = new RegExp(/TYPE=hidden/g);
    let QR = new RegExp(/['"]?QR_Button_[1-9]*['"]?/);
    let RD = new RegExp(/(?<=Description=").*(?=" Result)/);


    for (var i = 0; i < lines.length; i++) {
        lines[i] = lines[i].replace(QR, "'" + lines[i].match(QR) + "'");
        lines[i] = lines[i].replace(inputstart, '<input id=');

        if (lines[i].match(RD) != null) {
            dispoNames.push(lines[i].match(RD)[0]);
        }
        cleanDBs += lines[i] + "\n";
        dispoTextList += lines[i];
    }
    console.log("CLEAN DISPOSITIONS");
    console.log(cleanDBs);
    //clickCounter = 0;
    $("#cleanDBs").html(cleanDispositions);
    //$("#namesAndCategories").show();
}




function cleanDbField() {
    var input = $("#dirtyDBs").val();
    var cleanFields = "";

    //REGEX
    let inputStart = new RegExp(/<INPUT ID=/g);
    let type = new RegExp(/TYPE=/g);
    let amcatGroupAttr = new RegExp(/AMCATGROUPATTRIBUTES=/g);
    let endingTag = new RegExp(/>'>/g);


    cleanFields = input.replace(inputStart, "<input id=");
    cleanFields = cleanFields.replace(type, "type=");
    cleanFields = cleanFields.replace(amcatGroupAttr, "amcatgroupattributes=");
    cleanFields = cleanFields.replace(endingTag, ">'/>");

    console.log(cleanFields);
    $("#cleanDBs").html(cleanFields);
}


$(document).ready(function () {
    $("#filterBox").keyup(function () {
        for (var i = 0; i < $("#Content_disposByCampaign p").length; i++) {

            if ($("#Content_disposByCampaign p")[i].innerHTML.toUpperCase().includes($("#filterBox").val().toUpperCase())) {
                $("#Content_disposByCampaign p")[i].style = "display: block";
            } else {
                $("#Content_disposByCampaign p")[i].style = "display: none";
            }

        }
    })
})

$(document).ready(function () {
    $("#filterBox2").keyup(function () {
        for (var i = 0; i < $("#Content_fields p").length; i++) {

            if ($("#Content_fields p")[i].innerHTML.toUpperCase().includes($("#filterBox2").val().toUpperCase())) {
                $("#Content_fields p")[i].style = "display: block";
            } else {
                $("#Content_fields p")[i].style = "display: none";
            }

        }
    })
})

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

$(document).ready(function () {
    $("#filterBox3").keyup(delay(function (e) {
        for (var i = 0; i < $("#Content_fields p").length; i++) {

            if ($("#Content_fields p")[i].innerHTML.toUpperCase().includes($("#filterBox3").val().toUpperCase())) {
                $("#Content_fields p")[i].style = "display: block";
            } else {
                $("#Content_fields p")[i].style = "display: none";
            }

        }
    }, 200))
})


$(document).ready(function () {
    $("#filterBox4").keyup(function () {
        for (var i = 0; i < $(".searchDiv").length; i++) {

            if ($("#Content_fields .searchDiv")[i].innerHTML.toUpperCase().includes($("#filterBox4").val().toUpperCase())) {
                $("#Content_fields .searchDiv")[i].style = "display: block;";
            } else {
                $("#Content_fields .searchDiv")[i].style = "display: none;";
            }

        }
    })
})

function goTo(pageName) {
    location.href = pageName;
}

var count = 0;
function createNewEntry() { 
    $("#scriptContainer").append(
        "<div class='col-6 entryContainer' style='margin-bottom:10px; padding-left: 0px; display:inline-flex;' id='entry" + count + "'>"
        + "<input type='text' style='padding-left: 0px; width: 300px;' class='scriptEntries form-control' id='scriptName" + count + "' />"
        + "<img src='icons/trash-white/white-remove-50.png' class='entries' style='cursor:pointer; height:35px; width: auto;'/>"
        + "</div>");
    count += 1;
}

//Deletes script name entries
$(document).on('click', '.entries', function () {
    var pid = $(this).parent().attr("id");
    console.log(pid);
    var entry = document.getElementById(pid);
    entry.remove();
});

function createEverything() {
    $("#result").html("");
    var scriptname = $("#Content_scriptName").val();
    var scriptlist = [];
    $(".scriptEntries").each(function () {
        scriptlist.push($(this).val());
    });
    var confirmationString = "Are you sure you wish to create the following scripts?\n";
    confirmationString += (scriptname + "\n");
    $(".scriptEntries").each(function () {
        confirmationString += ($(this).val() + "\n");
    })

    if (confirm(confirmationString)) {
        console.log(scriptname);
        console.log(scriptlist);
        //console.log("{'scriptName':" + "'" + scriptName + "', 'scriptList':" + "[" + scriptList + "]" + "}");
        $.ajax({
            type: "POST",
            url: "createScript.aspx/CreateScripts_Web",
            data: JSON.stringify({ scriptName: scriptname, scriptList: scriptlist }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
               
                if (response.d.indexOf("Successfully created the following") !== -1) {
                    $("#result").html(response.d);
                    $("#result").attr("style", "color: green");
                    $("#Content_scriptName").val("");
                    $(".entryContainer").each(function () {
                        $(this).remove();
                    })
                } else {
                    $("#result").html(response.d);
                    $("#result").attr("style", "color:red");
                }
                console.log("Success: " + response);
            },
            error: function (response, jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown + ". " + response);
                $("#result").html("An error occured during the ajax or sproc call.");
                $("#result").attr("style", "color:red");
            }
        })
    }
}