﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="ECNSEditor.aspx.cs" Inherits="Script_Resources_2020.ECNSEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
    <div id="navigationHeader">
        <nav class="navbar navbar-expand-lg navbarHeader">
            <div class="container headerContainer">
                <a class="navbar-brand" href="#">Common JS Functions</a>
            </div>
        </nav>
    </div>
    <div id="navigationBar">
        <nav class="navbar navbar-expand-lg">
            <div class="container navContainer">
                <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                        <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                        <a class="nav-item nav-link" href="Dbclean.aspx">Db Cleaner</a>
                        <a class="nav-item nav-link" href="DateTimePicker.aspx">Date and Time Picker</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 style="text-align: center; color: white;">ECNS Editor
                </h2>
            </div>
        </div>
        <div class="row">
            <p>
                Select a script: 
             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                 <ContentTemplate>
                     <asp:DropDownList ID="ListOfScripts" runat="server" OnSelectedIndexChanged="ListOfScripts_SelectedIndexChanged">
                         <asp:ListItem Text="Scripts" Value="">Scripts</asp:ListItem>
                     </asp:DropDownList>
                 </ContentTemplate>
             </asp:UpdatePanel>

            </p>
        </div>
        <div class="row">
            <div id="namesOfScripts" runat="server">
            </div>
        </div>
        <div class="container" style="display: inline-flex">
            <div class="dispositionButtonContainer" id="categoryButtons" runat="server">
            </div>

            <div class="verticalLine">
            </div>

            <div id="DispoContainer" class="dispositionContainer" runat="server">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="AddressVerification" runat="server">
</asp:Content>
