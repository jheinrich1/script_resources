﻿<%@ Page Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="Dbclean.aspx.cs" Inherits="Script_Resources_2020.Dbclean" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
<div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">DB Field Cleaner</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                    <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                    <a class="nav-item nav-link" href="DateTimePicker.aspx">Date and Time Picker</a>
                    <a class="nav-item nav-link" href="js.aspx">JavaScript</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
<div class="content container">
    <!-- Dirty Disposition Input -->
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">Dirty DB Fields</span>
        </div>
        <textarea class="form-control" id="dirtyDBs" aria-label="With textarea">
     &lt;INPUT ID="CurrentAgentName" TYPE="hidden" AMCATGROUPATTRIBUTES='&lt;amcatgroup type="Text" fieldname="CurrentAgentName" strongvalidation="False" allownulls="True"&gt;&lt;/amcatgroup&gt;'&gt;

     &lt;INPUT ID="firstname" TYPE="hidden" AMCATGROUPATTRIBUTES='&lt;amcatgroup type="Text" tablename="ContactInfo" fieldname="firstname" strongvalidation="False" allownulls="True"&gt;&lt;/amcatgroup&gt;'&gt;

     &lt;INPUT ID="lastname" TYPE="hidden" AMCATGROUPATTRIBUTES='&lt;amcatgroup type="Text" tablename="ContactInfo" fieldname="lastname" strongvalidation="False" allownulls="True"&gt;&lt;/amcatgroup&gt;'&gt;
        </textarea>
    </div>
    <br />
    <button type="button" class="btn btn-lg btn-block" id="cleanButton" onclick="cleanDbField()">Clean</button>
    <br />
    <!-- Clean Disposition Output -->
    <div class="input-group">
        <div class="input-group-prepend textContainer">
            <span class="input-group-text">Clean DB Fields</span>
        </div>
        <textarea class="form-control textContainer" id="cleanDBs" aria-label="With textarea"></textarea>
    </div>
    <br />
    <button id="copy1" type="button" class="btn btn-lg btn-block" onclick="copyToClipboard('#cleanDispositions'); copySuccess('#label1');">Copy</button>
    <br />
    <br />
    <!-- Disposition Categories and Values -->
   <%-- <div class="row">
        <div class="col-6">
        <p>
            Enter disposition categories:
        </p>
        <div id="dispoNames">
            <div id='dispoGroup0' class='input-group mb-3'>
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon0">Category 1</span>
                </div>
                <input type="text" class="form-control categories" placeholder="Other, Interested, Member, CMS" id="group0" />
            </div>
        </div>
        <button id="addName" onclick="createName()" type="button" class="btn btn-lg">Add Category</button><button id="deleteName" type="button" class="btn btn-lg">Delete Category</button>
        </div>    
    </div>
    <br />
    <br />--%>

    <!-- Disposition Names -->
<%--    <div class="row">
        <div class="col-12">

            <div id="dispositionNames">

            </div>
            <div id="dispositionCategories">

            </div>

            <button id="namesAndCategories" onclick="createNameList();" type="button" class="btn btn-lg Hide">Assign Categories</button>
        </div>
    </div>
    <br />
    <br />--%>

   <%-- <div class="row Hide" style="display: none;">
        <div class="col-12">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Select Statement</span>
            </div>
            <textarea class="form-control textOutput" id="HTMLholder" aria-label="With textarea"></textarea>
        </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-12">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">HTML</span>
                </div>
                <textarea class="form-control textOutput" id="HTMLcontainer" aria-label="With textarea"></textarea>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-12">--%>
            <!-- JS-->
           <%-- <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">JS</span>
                </div>
                <textarea class="form-control textJS" id="JSHolder" aria-label="With textarea"></textarea>
            </div>
        </div>
    </div>
    <br />--%>
    <!--P Tag checkbox -->
   <%-- <div id="ptagHolder">
        <p id="ptagText">
            Add &lt;p&gt; tags:
        </p>
        <input type="checkbox" value="" id="ptagButton" />
    </div>
    <br />--%>
    <%--<button id="getCode" onclick="checkSelections(); createMainCategoryList(); getHTMLCode(); updateContainer(); getJS();" type="button" class="btn btn-dark btn-lg">Get HTML and JS</button>--%>
   <%-- <button id="getCode" onclick="checkSelections();" type="button" class="btn btn-lg">Get HTML and JS</button>--%>



    <%--<button id="label1" type="button" class="btn btn-dark btn-lg btn-block" onclick="copyToClipboard('#cleanDispositions'); copySuccess('#label1');">Copy</button>--%>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="AddressVerification" runat="server">
</asp:Content>
