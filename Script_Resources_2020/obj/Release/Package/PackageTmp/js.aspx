﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="js.aspx.cs" Inherits="Script_Resources_2020.js" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="BannerName" runat="server">
    
</asp:content>
<asp:content id="Content3" contentplaceholderid="MenuLinkHolder" runat="server">
   <div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">Common JS Functions</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                    <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                    <a class="nav-item nav-link" href="Dbclean.aspx">Db Cleaner</a>
                    <a class="nav-item nav-link" href="DateTimePicker.aspx">Date and Time Picker</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:content>
<asp:content id="Content4" contentplaceholderid="PageNameHolder" runat="server">
   
</asp:content>
<asp:content id="Content5" contentplaceholderid="Content" runat="server">
<div class="container">
     <h4>Dropdown Show/Hide <button id="label1" type="button" class="btn btn-outline-dark btn-sm" onclick="copyToClipboard('#code1'); copySuccess('#label1');"><img src="icons/copy-50.png"  height="30" width="30"/></button></h4>
    <pre>
    <code class="language-js" contenteditable="true" id="code1">
    function showHide(selectID, option1, option2) {
        var selectID1 = "#" + selectID;
        var option1a = "#" + option1;
        var option2a = "#" + option2;

        $(document).ready(function () {
            $(selectID1).change(function () {
                if (option2a === "") {
                    if ($(selectID1).val() === "true") {
                        $(option1a).show();
                    }
                    else {
                        $(option1a).hide();
                    }
                } else {
                    if ($(selectID1).val() === "true") {
                        $(option1a).show();
                        $(option2a).hide();
                    }
                    else if ($(selectID1).val() === "false") {
                        $(option1a).hide();
                        $(option2a).show();
                    }
                    else {
                        $(option1a).hide();
                        $(option2a).hide();
                    }
                }
            }).trigger('change');
        })
    };

    showHide("testID", "yesTestID", "noTestID");
    </code>
    </pre>
    <br />
    <h4>Radio Show/Hide <button id="label2" type="button" class="btn btn-outline-dark btn-sm" onclick="copyToClipboard('#code2'); copySuccess('#label2');"><img src="icons/copy-50.png"  height="30" width="30"/></button></h4>
    <pre>
    <code class="language-js" contenteditable="true" id="code2">
    function radioShowHide(Container, Name, yesShow, noShow) {
        var radioContainer = "." + Container;
        var radioName = "input[name='" + Name + "']:checked";
        var yesDisplay = "#" + yesShow;
        var noDisplay = "#" + noShow;

        $(document).ready(function () {
            $(radioContainer).click(function () {
                var radioValue = $(radioName).val();
                if (radioValue == "true") {
                    $(yesDisplay).show();
                    $(noDisplay).hide();
                } else if (radioValue == "false") {
                    $(yesDisplay).hide();
                    $(noDisplay).show();
                }
            })
        })
    };

    radioShowHide("radioContainer", "radioTest", "yesDisplay", "noDisplay");
    </code>
    </pre>
    <br />
    <h4>Special Characters Validation <button id="label3" type="button" class="btn btn-outline-dark btn-sm" onclick="copyToClipboard('#code3'); copySuccess('#label3');"><img src="icons/copy-50.png"  height="30" width="30"/></button></h4>
    <pre>
    <code class="language-js" contenteditable="true" id="code3">
    function specialCharValidation(inputField) {
        var input = "#" + inputField;
        $(document).ready(function () {
            $(input).keypress(function (e) {
                var keyCode = e.which;
                if (keyCode == 34 || keyCode == 124 || keyCode == 47 || keyCode == 92 || keyCode == 39) {
                    e.preventDefault();
                }
            })
        })
    };

    specialCharValidation("testInput");
    </code>
    </pre>
    <br />
    <h4>Date Validation and Masking <button id="label4" type="button" class="btn btn-outline-dark btn-sm" onclick="copyToClipboard('#code4'); copySuccess('#label4');"><img src="icons/copy-50.png"  height="30" width="30"/></button></h4>
    <pre>
    <code class="language-js" contenteditable="true" id="code4">
    function validateDate(date) {
        var date_regex = /^\d{2}\/\d{2}\/\d{4}$/;
        var dateVal = "#" + date;
        $(document).ready(function () {
            var input = document.getElementById(date);

            var dateInputMask = function dateInputMask(elm) {
                $(elm).keypress(function (e) {
                    //elm.addEventListener('keypress', function (e) {
                    if (e.keyCode < 47 || e.keyCode > 57) {
                        e.preventDefault();
                    }
                    var len = elm.value.length;
                    // If we're at a particular place, let the user type the slash
                    // i.e., 12/12/1212
                    if (len !== 1 || len !== 3) {
                        if (e.keyCode == 47) {
                            e.preventDefault();
                        }
                    }
                    // If they don't add the slash, do it for them...
                    if (len === 2) {
                        elm.value += '/';
                    }

                    // If they don't add the slash, do it for them...
                    if (len === 5) {
                        elm.value += '/';
                    }
                });
            };
            dateInputMask(input);
            $(dateVal).focusout(function () {
                var Date = document.getElementById(date).value;
                if (!date_regex.test(Date)) {
                    alert("Date incorrectly formatted. Use mm/dd/yyyy");
                };
            })
        })
    };

    validateDate("testDate");
    </code>
    </pre>
</div>
</asp:content>
<asp:content id="Content6" contentplaceholderid="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:content>
<asp:content id="Content7" contentplaceholderid="AddressVerification" runat="server">
</asp:content>
