﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace Script_Resources_2020
{
    public class Client
    {
        public int? ClientId { get; set; }
        public string ClientName { get; set; }
        public string DatabaseName { get; set; }
        public string TABLE_NAME { get; set; }
        public string COLUMN_NAME { get; set; }
        public string DATA_TYPE { get; set; }
        public static string Error { get; set; }

        public static List<Client> GetClients()
        {
            var clients = new List<Client>();
            Error = "";
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterManager"].ConnectionString))
            using (var command = new SqlCommand("usp_Bloom_GetClients", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    conn.Open();
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var client = new Client
                            {
                                ClientId = Convert.ToInt32(dr["PKBloomClientID"]),
                                ClientName = dr["Name"].ToString(),
                                DatabaseName = dr["DatabaseName"].ToString()
                            };
                            clients.Add(client);
                        }
                        dr.Close();
                    }
                }
                catch (SqlException se)
                {
                    Error = se.Message;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                }
            }

            return clients;
        }

        public static List<Client> GetFieldsForClient(string DBName)
        {
            var clients = new List<Client>();

            using (var conn = new SqlConnection("Data Source=ccsprdsql;Initial Catalog=" + DBName + ";Persist Security Info=True;User ID=web_users;Password=mUu5TsT%"))
            {
                var cmd = new SqlCommand("usp_ScriptEditor_GetTblColumns", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@pTblName", SqlDbType.NVarChar)).Value = "ContactInfo";
                cmd.Parameters.Add(new SqlParameter("@pCat", SqlDbType.NVarChar)).Value = DBName;

                var count = 1;
                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var client = new Client
                            {
                                TABLE_NAME = dr["TABLE_NAME"].ToString(),
                                COLUMN_NAME = dr["COLUMN_NAME"].ToString(),
                                DATA_TYPE = dr["DATA_TYPE"].ToString()
                            };
                            clients.Add(client);
                        }
                    }
                }
                catch (Exception)
                {
                    return clients;
                }

            }
            return clients;

        }
    }
}