﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Web.UI.WebControls;
using System.Windows;

namespace Script_Resources_2020
{
    public partial class getProjects : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RenderScripts();
            }
        }

        protected void RenderScripts()
        {
            try
            {
                ddScript.Items.Clear();
                var scriptList = Script.GetScripts().OrderBy(a => a.ScriptName).ToList();

                var emptyItem = new ListItem
                {
                    Text = "",
                    Value = ""
                };

                ddScript.Items.Add(emptyItem);
                foreach (var c in scriptList)
                {
                    var item = new ListItem
                    {
                        Text = c.ScriptName,
                        Value = c.ScriptID.ToString()
                    };
                    ddScript.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        public void ddScript_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> returnVal = new List<string>();
            List<string> campaigns = new List<string>();
            try
            {
                if (ddScript.SelectedValue.Equals(""))
                {
                    fieldSearch.Visible = false;
                    fields.InnerHtml = "";
                    return;
                }
                var campaignList = Campaign.GetCampaignsByScriptName(ddScript.SelectedItem.Text);
                var DBsByCampaign = Campaign.GetCampaignsAndDBs();
                foreach (var campaign in campaignList)
                {
                    returnVal.Add("<div class='searchDiv'>");
                    returnVal.Add("<p class='extras' style='font-weight: bold; font-size: 15px !important; margin-bottom: 0px;'>" + campaign.CampaignName + "</p>");
                    campaigns.Add(campaign.CampaignName);

                   
                    foreach (var record in DBsByCampaign)
                    {
                        if (campaign.CampaignName.Contains(record.CampaignName))
                        {
                            returnVal.Add("<small style='margin-left: 0.4rem; margin-bottom: 0.4rem;'>" + record.DatabaseName + "</small>");
                            break;
                        }
                    }
                    returnVal.Add("</div>");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }

            fields.InnerHtml = "";
            foreach (string i in returnVal)
            {
                fields.InnerHtml += i;
            }

            fieldSearch.Visible = true;

            
        }
    }
}