﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class ECNS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetDispositions("1250");
        }

        public void GetDispositions(string scriptId)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterStaging"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_Bloom_GetDispositionByScriptID", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@ScriptID", SqlDbType.NVarChar)).Value = scriptId;

                conn.Open();

                GridView1.DataSource = cmd.ExecuteReader();
                GridView1.DataBind();
            }
        }

        public class Dispo
        {
            public int CallResultCode { get; set; }
            public string CallResultDescription { get; set; }
            public string DispCategory { get; set; }
        }

        [System.Web.Services.WebMethod]
        public static string GetDispoList(string scriptId)
        {
            List<Dispo> dispoList = new List<Dispo>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterStaging"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_Bloom_GetDispositionByScriptID", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@ScriptID", SqlDbType.NVarChar)).Value = scriptId;

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Dispo dispo = new Dispo();
                            dispo.CallResultCode = Convert.ToInt32(dr["CallResultCode"]);
                            dispo.CallResultDescription = dr["CallResultCodeDescription"].ToString();
                            dispo.DispCategory = dr["DispositionCategory"].ToString();
                            dispoList.Add(dispo);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return "error: " + ex.Message;
                }

            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(dispoList);

        }
    }
}