﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class scriptNames : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            namesOfScripts.InnerHtml = "";
            namesOfCampaigns.InnerHtml = "";
            scriptsByCampaign.InnerHtml = "";
   
            
            foreach (string i in GetScripts())
            {
                namesOfScripts.InnerHtml += i;
            }

            foreach (string i in GetCampaigns())
            {
                namesOfCampaigns.InnerHtml += i;
            }

            //foreach (string i in GetDisposForCampaign("Advise_CS_OB"))
            //{
            //    disposByCampaign.InnerHtml += i;
            //}

            foreach (string i in GetScriptsForCampaign(347))
            {
                scriptsByCampaign.InnerHtml += i;
            }

            foreach (string i in GetCampaignsDropdown())
            {
                listOfCampaigns.Items.Add(i);
            }

        }

        public static List<string> GetScripts()
        {
            
            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_ScriptEditor_GetScriptFolderNames", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            //<input id="customerid" readonly="readonly" style="background-color: Transparent; border: 0" amcatgroupattributes='<AmcatGroup Type="Text" TableName="ContactInfo" FieldName="PKCustomerRecord" StrongValidation="False" AllowNulls="True"></AmcatGroup>'>
                            returnVal.Add("<p class='extras'>" + "&lt;input id='customerid' readonly='readonly' amcatgroupattributes='<AmcatGroup Type='Text' TableName='ContactInfo'" + dr[0] + "</p>");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public static List<string> GetCampaigns()
        {

            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_GetCampaignNamesAndKeys", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("<p class='extras'> Name: " + dr[1] + ".   ID: " + dr[0] + "</p>");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }


        public static List<string> GetDisposForCampaign(string campaignName)
        {
            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_ScriptEditor_GetCampaignResultCodes", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@pCampaignName", SqlDbType.NVarChar)).Value = campaignName;
                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("<p class='extras'>" + dr[0] + "</p>");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }


        public static List<string> GetScriptsForCampaign(int campaignID)
        {
            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_Agent_GetScriptsForCampaign", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@CampaignID", SqlDbType.NVarChar)).Value = campaignID;
                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("<p class='extras'>" + dr[0] + "</p>");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public static List<string> GetCampaignsDropdown()
        {

            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_GetCampaignNamesAndKeys", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("" + dr[1] + "");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public void getDisposForSelectedCampaign(Object sender, EventArgs e)
        {
            var disposByCampaignn = GetDisposForCampaign(listOfCampaigns.SelectedValue);

            foreach (string i in disposByCampaignn)
            {
                disposByCampaign.InnerHtml += i;
            }

        }

    }
}