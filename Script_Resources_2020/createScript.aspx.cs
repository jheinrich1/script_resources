﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows;
using System.Net.Mail;
using System.Web.UI;
using System.Diagnostics;
using System.Windows.Controls;
using System.Web.UI.HtmlControls;
using Button = System.Web.UI.WebControls.Button;
using Microsoft.SqlServer.Server;
using TextBox = System.Web.UI.WebControls.TextBox;
using Panel = System.Web.UI.WebControls.Panel;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Runtime.Remoting.Contexts;
using Tools;

namespace Script_Resources_2020
{
    public partial class createScript : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userObject = System.Security.Principal.WindowsIdentity.GetCurrent();
            curUser.Value = userObject.Name.Replace("RMGCOM\\", "");

            using (new Impersonator("jheinrich", "RMGCOM", "Officevibes!"))
            {
                var userObject2 = System.Security.Principal.WindowsIdentity.GetCurrent();
                curUser.Value = userObject2.Name.Replace("RMGCOM\\", "");
            }
        }

        public static List<string> GetExistingScripts()
        {
            using (new Impersonator("jheinrich", "RMGCOM", "Officevibes!"))
            {
                string directoryPath = @"\\staging.scripts.rmgcom.local\Staging Scripts\";
                //string directoryPath = "C:\\Staging Scripts\\";
                List<string> existingScripts = new List<string>();

                try
                {
                    foreach (string folder in Directory.EnumerateDirectories(directoryPath))
                    {
                        existingScripts.Add(folder.Replace(@"\\staging.scripts.rmgcom.local\Staging Scripts\", "").ToLower());
                        //existingScripts.Add(folder.Replace("C:\\Staging Scripts\\", "").ToLower());
                    }
                }
                catch (Exception ex)
                {

                }
                return existingScripts;
            }
        }


        [WebMethod]
        public static string CreateScripts_Web(string scriptName, List<string> scriptList)
        {
            
                try
                {
                    if (scriptName.Equals("") || scriptName.Equals(" "))
                    {
                        return "Please provide a script name.";
                    }

                    List<string> existingScripts = GetExistingScripts();

                    if (existingScripts.Contains(scriptName.ToLower()))
                    {
                        return scriptName + " already exists.";
                    }

                    List<string> masterScriptList = new List<string>();
                    masterScriptList = scriptList;

                    masterScriptList.Add(scriptName);

                    if (masterScriptList.Count() != masterScriptList.Distinct().Count())
                    {
                        return "Cannont create two scripts with the same name.";
                    }

                    scriptList.RemoveAt(scriptList.Count() - 1);

                    bool breakout = false;
                    string duplicate = "";
                    for (var i = 0; i < scriptList.Count; i++)
                    {
                        if (breakout == true)
                        {
                            break;
                        }
                        if (existingScripts.Contains(scriptList[i].ToLower()) && breakout == false)
                        {
                            breakout = true;
                            duplicate = scriptList[i];
                            //return scriptList[i] + " already exists.";
                        }
                    }

                    if (breakout == true)
                    {
                        return duplicate + " already exists.";
                    }

                    string[] startHTMContent =
                            {
                        "<html>", "<head>", "</head>", "<body>", "</body>", "</html>"
                    };

                //using (new Impersonator("jheinrich", "RMGCOM", "Officevibes!"))
                //{
                    //System.IO.Directory.CreateDirectory("C:\\Staging Scripts\\" + scriptName);
                    System.IO.Directory.CreateDirectory(@"\\staging.scripts.rmgcom.local\Staging Scripts\" + scriptName);
                    //Add start.htm file to newly created script folder. Populate start.htm with HTML skeleton
                    //File.WriteAllLines(@"\\staging.scripts.rmgcom.local\Staging Scripts\" + scriptName + "\\start.htm", startHTMContent);
                    //File.WriteAllLines("C:\\Staging Scripts\\" + scriptName + "\\start.htm", startHTMContent);
                //}
                    string scriptNames = scriptName + ", ";
                    string finalscriptNames = "";
                    for (var i = 0; i < scriptList.Count(); i++)
                    {
                        scriptNames += (scriptList[i] + ", ");
                    }
                    finalscriptNames = scriptNames.Remove(scriptNames.Count() - 2, 1);

                    //Add script to Scripts table in CallCenter db.
                    //Script.AddScriptToDB(scriptname);
                    //createScript c1 = new createScript();
                    //c1.AddScriptsToDB(scriptName, scriptList);
                    //Add confirm prompt before creating script.
                    //Create Jira ticket
                    //c1.CreateTicket(scriptName, scriptList);

                    return "Successfully created the following script(s):\n" + finalscriptNames;
                }
                catch (Exception ex)
                {
                    return "Error: " + ex;
                }
            
            
        }

        public void AddScriptsToDB(string scriptname, List<string> scriptList)
        {
            Script.AddScriptToDB(scriptname);
            if (scriptList.Count() > 0)
            {
                for (var i = 0; i < scriptList.Count(); i++)
                {
                    Script.AddScriptToDB(scriptList[i]);
                }
            }
        }

        public void CreateTicket(string scriptName, List<string> scriptList)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "docker01.rmgcom.local";
            
            MailMessage message = new MailMessage();
            message.From = new MailAddress("jheinrich@bloominsurance.com");
            message.To.Add("itsupport@bloominsurance.com");
            message.Subject = "Script to Site in IIS - TEST";
            string scriptNames = scriptName + "\n";
            for (var i=0; i < scriptList.Count(); i++)
            {
                scriptNames += (scriptList[i] + "\n");
            }
            message.Body = "Please create the following script(s) as sites in IIS UAT:\n" + scriptNames + "\nThank you!";
            client.Send(message);
        }


    }
}