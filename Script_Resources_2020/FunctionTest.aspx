﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="FunctionTest.aspx.cs" Inherits="Script_Resources_2020.FunctionTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
    <div id="navigationHeader">
        <nav class="navbar navbar-expand-lg navbarHeader">
            <div class="container headerContainer">
                <a class="navbar-brand" href="#">Function Test</a>
            </div>
        </nav>
    </div>
    <div id="navigationBar">
        <nav class="navbar navbar-expand-lg">
            <div class="container navContainer">
                <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                        <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                        <a class="nav-item nav-link" href="Dbclean.aspx">Db Cleaner</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <p>
        This is a test
        <select id="Example" onchange="showHide1($(this).val(),'Example','True','False')">
            <option value=""></option>
            <option value="op1">Yes</option>
            <option value="op2">No</option>
        </select>
    </p>
    <div id="Trueop1" class="Hide">
        <p>
            <b>True</b>
            <select id="DDTrue" onchange="showHide1($(this).val(),'DDTrue', 'True1','False1','Other1','Unknown1')">
                <option value=""></option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <option value="Other">Other</option>
                <option value="Unknown">Unknown</option>
            </select>
        </p>
        <div id="True1Yes" class="Hide">
            <p>
                True 1
            </p>
        </div>
        <div id="False1No" class="Hide">
            <p>
                <b>False</b>
                <select id="DDFalse" onchange="showHide1($(this).val(),'DDFalse','One','Two','Three','Four')">
                    <option value=""></option>
                    <option value="op1">Yes</option>
                    <option value="op2">No</option>
                    <option value="op3">Other</option>
                    <option value="op4">Unknown</option>
                </select>
            </p>
            <div id="Oneop1" class="Hide">
                <p>
                    1
                </p>
            </div>
            <div id="Twoop2" class="Hide">
                <p>
                    2
                </p>
            </div>
            <div id="Threeop3" class="Hide">
                <p>
                    3
                </p>
            </div>
            <div id="Fourop4" class="Hide">
                <p>
                    4
                </p>
            </div>
        </div>
        <div id="Other1Other" class="Hide">
            <p>
                Other 1
            </p>
        </div>
        <div id="Unknown1Unknown" class="Hide">
            <p>
                Unknown 1
            </p>
        </div>
    </div>
    <div id="Falseop2" class="Hide">

        <div id="True2true" class="Hide">
            <p>
                True 2
            </p>
        </div>
        <div id="False2false" class="Hide">
            <p>
                False 2
            </p>
        </div>
    </div>

    <!----------------------------------------------------------------->

    <script>
        function showHide1(value) {
            let obj = {}; //New Object
            var selectID1 = "#" + arguments[1]; //Passed ID from Arguments[1]

            //To Lesson Confusion. The first option is option2.
            for (let i = 2; i < arguments.length; i++) {
                obj['option' + i] = "#" + arguments[i];
            }
            $(document).ready(function () {
                $(selectID1).change(function () {
                    //-------------------------If ONE option(s) ------------------------------------
                    if (obj.option3 === undefined) {
                        if ($(this).val() === "op1" ||
                            $(this).val() === "Yes" ||
                            $(this).val() === "No" ||
                            $(this).val() === "false" ||
                            $(this).val() === "true" ||
                            $(this).val() === "1" ||
                            $(this).val() === "0") {
                            $(obj.option2 + value).show();
                        }
                        else {
                            $(obj.option2 + value).hide();
                        }
                    }
                    //------------------------------------------------------------------------------

                    //-------------------------If TWO option(s) ------------------------------------
                    else if (obj.option4 === undefined) {
                        if ($(this).val() === "op1" ||
                            $(this).val() === "Yes" ||
                            $(this).val() === "true" ||
                            $(this).val() === "1") {
                            $(obj.option2 + value).show();
                            $(obj.option3 + value).hide();
                        }
                        else if ($(this).val() === "op2" ||
                            $(this).val() === "No" ||
                            $(this).val() === "false" ||
                            $(this).val() === "0") {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).show();
                        }
                        else {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).hide();
                        }
                    }
                    //------------------------------------------------------------------------------

                    //-------------------------If THREE option(s) ------------------------------------
                    else if (obj.option5 === undefined) {
                        if ($(this).val() === "op1" ||
                            $(this).val() === "Yes" ||
                            $(this).val() === "true" ||
                            $(this).val() === "1") {
                            $(obj.option2 + value).show();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).hide();
                        }
                        else if ($(this).val() === "op2" ||
                            $(this).val() === "No" ||
                            $(this).val() === "false" ||
                            $(this).val() === "0") {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).show();
                            $(obj.option4 + value).hide();
                        }
                        else if ($(this).val() === "op3" ||
                            $(this).val() === "Other") {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).show();
                        }
                        else {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).hide();
                        }
                    }
                    //------------------------------------------------------------------------------

                    //-------------------------If FOUR option(s) ------------------------------------
                    else if (obj.option6 === undefined) {
                        if ($(this).val() === "op1" ||
                            $(this).val() === "Yes" ||
                            $(this).val() === "true" ||
                            $(this).val() === "1") {
                            $(obj.option2 + value).show();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).hide();
                            $(obj.option5 + value).hide();
                        }
                        else if ($(this).val() === "op2" ||
                            $(this).val() === "No" ||
                            $(this).val() === "false" ||
                            $(this).val() === "0") {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).show();
                            $(obj.option4 + value).hide();
                            $(obj.option5 + value).hide();
                        }
                        else if ($(this).val() === "op3" ||
                            $(this).val() === "Other") {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).show();
                            $(obj.option5 + value).hide();
                        }
                        else if ($(this).val() === "op4" ||
                            $(this).val() === "Unknown") {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).hide();
                            $(obj.option5 + value).show();
                        }
                        else {
                            $(obj.option2 + value).hide();
                            $(obj.option3 + value).hide();
                            $(obj.option4 + value).hide();
                            $(obj.option5 + value).hide();
                        }
                    }
                    //------------------------------------------------------------------------------
                }).trigger('change');
            })
        };
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="AddressVerification" runat="server">
</asp:Content>
