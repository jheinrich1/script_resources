﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class ECNSEditor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //namesOfScripts.InnerHtml = "";

            //foreach (string i in GetScripts())
            //{
            //    namesOfScripts.InnerHtml += i;
            //}
            AddScriptsToDropdown();
        }

        public static List<string> GetScripts()
        {

            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_GetScriptNamesAndKeys", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("<p class='extras'>" + dr[1] + "</p>");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public void AddScriptsToDropdown()
        {
            Dictionary<string, string> dicScripts = new Dictionary<string, string>();
            List<string> carrierList = new List<string>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_GetScriptNamesAndKeys", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr != null)
                        {
                            while (rdr.Read())
                            {
                                dicScripts.Add(rdr[0].ToString(), rdr[1].ToString());
                            }
                        }
                    }
                    conn.Close();

                }
                catch (SqlException se)
                {

                }
                catch (Exception ex)
                {
                }
            }

            var mySortedList = dicScripts.OrderBy(d => d.Value).ToList();

            ListOfScripts.DataSource = mySortedList;
            ListOfScripts.DataTextField = "Value";
            ListOfScripts.DataValueField = "Key";
            ListOfScripts.DataBind();
        }

        public class Dispo
        {
            public int CallResultCode { get; set; }
            public string CallResultDescription { get; set; }
            public string DispCategory { get; set; }
        }

        public static List<Dispo> GetDispos(string scriptId)
        {
            List<Dispo> dispoList = new List<Dispo>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterStaging"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_Bloom_GetDispositionByScriptID", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@ScriptID", SqlDbType.NVarChar)).Value = scriptId;

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Dispo dispo = new Dispo();
                            dispo.CallResultCode = Convert.ToInt32(dr["CallResultCode"]);
                            dispo.CallResultDescription = dr["CallResultCodeDescription"].ToString();
                            dispo.DispCategory = dr["DispositionCategory"].ToString();
                            dispoList.Add(dispo);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return dispoList;
        }

        public void ListOfScripts_SelectedIndexChanged(object sender, EventArgs e)
        {
            var scriptName = ListOfScripts.SelectedItem.Value;
            var scriptKey = ListOfScripts.SelectedValue;
            GetDispos(scriptKey);

        }

    }
}