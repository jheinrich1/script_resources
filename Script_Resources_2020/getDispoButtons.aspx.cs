﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class getDispoButtons : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //listOfCampaigns.Items.Clear();

            foreach (string i in GetCampaignsDropdown())
            {
                listOfCampaigns.Items.Add(i);
            }

        }


       

        public static List<string> GetDisposForCampaign(string campaignName)
        {
            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_ScriptEditor_GetCampaignResultCodes", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@pCampaignName", SqlDbType.NVarChar)).Value = campaignName;

                var count = 1;
                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("<button id=\"QR_Button_" + count + "\" amcatgroupattributes=\"<AmcatGroup Type='QuickResult' ResultDescription='" + dr[0] + "' ResultCode='" + dr[1] + "' ></AmcatGroup>\"><font color =\"red\">" + dr[0] + "</font></button><br/>");
                            count += 1;
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }


        public static List<string> GetCampaignsDropdown()
        {

            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_GetCampaignNamesAndKeys", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("" + dr[1] + "");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public void getDisposForSelectedCampaign(Object sender, EventArgs e)
        {
            var disposByCampaignn = GetDisposForCampaign(listOfCampaigns.SelectedValue);
            disposByCampaign.InnerHtml = "";
            foreach (string i in disposByCampaignn)
            {
                disposByCampaign.InnerHtml += i;
            }

        }

    }
}