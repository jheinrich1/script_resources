﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class getDispos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                List<string> campaigns = GetProjects();

                campaigns.Sort();

                var emptyItem = new ListItem
                {
                    Text = "",
                    Value = ""
                };

                listOfCampaigns.Items.Add(emptyItem);

                foreach (string campaign in campaigns.Distinct())
                {
                    listOfCampaigns.Items.Add(campaign);
                }

               
            }

        }

        public static List<string> GetProjects()
        {
            var projects = new List<string>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterManager"].ConnectionString))
            using (var command = new SqlCommand("usp_Bloom_GetProjectList", conn))
            {
                command.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    

                    using (var dr = command.ExecuteReader())
                    {
                       
                        
                        while (dr.Read())
                        {

                            projects.Add(dr["Name"].ToString());
                        }

                        dr.NextResult();
                        while (dr.Read())
                        {

                            projects.Add(dr["Name"].ToString());
                        }
                        dr.Close();
                    }
                }
                catch (SqlException se)
                {
                    
                }
                catch (Exception ex)
                {
                    
                }
            }

            return projects;
        }


        public static List<string> GetDisposForCampaign(string campaignName)
        {
            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_ScriptEditor_GetCampaignResultCodes", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@pCampaignName", SqlDbType.NVarChar)).Value = campaignName;

                var count = 1;
                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("<p class='extras'>" + "&lt;button id=\"QR_Button_" + count + "\" amcatgroupattributes=\"&lt;AmcatGroup Type='QuickResult' ResultDescription='" + dr[0] + "' ResultCode='" + dr[1] + "' &gt;&lt;/AmcatGroup>\">&lt;font color =\"red\">" + dr[0] + "&lt;/font>&lt;/button>" + "</p>");
                            count += 1;
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }


        public static List<string> GetCampaignsDropdownOB()
        {

            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_GetCampaignNamesAndKeys", conn) { CommandType = CommandType.StoredProcedure };

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("" + dr[1] + "");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public static List<string> GetCampaignsDropdownIB()
        {

            List<string> returnVal = new List<string>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterManager"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_Bloom_GetProjectList", conn) { CommandType = CommandType.StoredProcedure };

                //cmd.Parameters.Add(new SqlParameter("@ActiveOnly", SqlDbType.Int).Value = 1;

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnVal.Add("" + dr[1] + "");
                        }
                    }
                }
                catch (Exception)
                {
                    return returnVal;
                }

            }

            returnVal.Sort();
            return returnVal;

        }

        public void getDisposForSelectedCampaign(Object sender, EventArgs e)
        {
            if (listOfCampaigns.SelectedValue.Equals(""))
            {
                disposByCampaign.Visible = false;
                disposByCampaign.InnerHtml = "";
                return;
            }
            var disposByCampaignn = GetDisposForCampaign(listOfCampaigns.SelectedValue);
            disposByCampaign.InnerHtml = "";
            foreach (string i in disposByCampaignn)
            {
                disposByCampaign.InnerHtml += i;
            }
            disposByCampaign.Visible = true;
            searchFilter.Visible = true;
        }
    }
}