﻿using Resources_Base.EmailTemplates;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class SendEmail : System.Web.UI.Page
    { 
       protected void Page_Load(object sender, EventArgs e)

        {



        }
        //Works in Gmail
        protected void SubmitBtn_OnClick(object sender, EventArgs e)
        {
            DateTime currentTime = DateTime.Now;
            var eventType = "Confirmation";
            var header = "Joe Test";
            SendGridApiService.ATTACHMENTS.Clear();

            var callDateTime = " " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(currentTime, TimeZoneInfo.Local.Id, "Eastern Standard Time");
            SendGridApiService.TEMPLATE_ID = "5a7841b7-a0b2-4e4f-a0af-b5666bc6bc6c";
            SendGridApiService.FROM_EMAIL = "jheinrich@bloominsurance.com";
            SendGridApiService.FROM_NAME = "Joe H Test";
            Byte[] filePath = File.ReadAllBytes(Server.MapPath(@"~\test.pdf"));
            string base64EncodedPdf = Convert.ToBase64String(filePath);
            string pdfString = "Hello";
            var substitutions = new Dictionary<string, string>
            {
                {"[Insert Event Header here]", header},
                {"[Insert Event Type here]", eventType}
            };


            Attachment att = new Attachment
            {
                Filename = "test.pdf",
                Content = base64EncodedPdf,
                Type = "application/pdf",
                Disposition = "inline",
                ContentId = "pdfAttachment"
            };

            SendGridApiService.ATTACHMENTS.Add(att);

            try
            {
                //SendGridApiService.SendEmailWithAttachment(brokerEmail, "Aetna ready to sell broker flyer 2023.pdf", substitutions, header, false);
                bool result = SendGridApiService.SendEmailWithAttachment("jheinrich@bloominsurance.com", "Test2.pdf", substitutions, "Joe Testing").Result;
                if (result == true)
                {
                    lblSuccess.Visible = true;
                    lblSuccess.Text = "Email sent successfully.";
                } else
                {
                    lblSuccess.Visible = true;
                    lblSuccess.Text = "Failure.";
                }

            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = "Error sending email. Please contact IT. Error message: " + ex.Message;
            }

        }
    }
}