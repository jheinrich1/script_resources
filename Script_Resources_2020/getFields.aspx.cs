﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Web.UI.WebControls;
using System.Windows;

namespace Script_Resources_2020
{
    public partial class getFields : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RenderClients();
            }
        }

        protected void RenderClients()
        {
            try
            {
                ddClient.Items.Clear();
                var clientsList = Client.GetClients();

                var emptyItem = new ListItem
                {
                    Text = "",
                    Value = ""
                };

                ddClient.Items.Add(emptyItem);
                foreach (var c in clientsList)
                {
                    var item = new ListItem
                    {
                        Text = c.ClientName,
                        Value = c.DatabaseName.ToString()
                    };
                    ddClient.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        public void ddClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> returnVal = new List<string>();
            try
            {
                if (ddClient.SelectedValue.Equals("")) {
                    fieldSearch.Visible = false;
                    fields.InnerHtml = "";
                    return;
                }
                var fieldList = Client.GetFieldsForClient(ddClient.SelectedValue);

                foreach (var field in fieldList)
                {
                    returnVal.Add("<p class='extras'>&lt;input id='" + field.COLUMN_NAME + "' amcatgroupattributes = '&lt;AmcatGroup Type=\"Text\" TableName=\"ContactInfo\" FieldName=\"" + field.COLUMN_NAME + "\" StrongValidation=\"False\" AllowNulls=\"True\"&gt;&lt;/AmcatGroup&gt;' /&gt;</p>");
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }

            fields.InnerHtml = "";
            foreach (string i in returnVal)
            {
                fields.InnerHtml += i;
            }

            fieldSearch.Visible = true;
        }
    }
}