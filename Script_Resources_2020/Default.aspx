﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Script_Resources_2020.Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="BannerName" runat="server">
    
</asp:content>
<asp:content id="Content3" contentplaceholderid="MenuLinkHolder" runat="server">
   <div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">Script Resources</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="getFields.aspx">Amcat Fields by Client</a>
                    <a class="nav-item nav-link" href="getProjects.aspx">Projects & DB's by Script</a>
                    <a class="nav-item nav-link" href="getDispos.aspx">Dispositions by Campaign</a>
                    <a class="nav-item nav-link" href="getScriptIDs.aspx">Script ID's</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:content>
<asp:content id="Content4" contentplaceholderid="PageNameHolder" runat="server">
   
</asp:content>
<asp:content id="Content5" contentplaceholderid="Content" runat="server">
<div class="container">
    <div class="row mb-3">
       <button type="button" class="btn btn-light btn-lg btn-block" onclick="goTo('getScriptIDs.aspx')">Script ID's</button>
   </div>
   <div class="row mb-3">
       <button type="button" class="btn btn-light btn-lg btn-block" onclick="goTo('getFields.aspx')">Amcat Fields by Client</button>
   </div>
    <div class="row mb-3">
       <button type="button" class="btn btn-light btn-lg btn-block" onclick="goTo('getDispos.aspx')">Dispositions by Campaign</button>
   </div>
   <div class="row mb-3">
       <button type="button" class="btn btn-light btn-lg btn-block" onclick="goTo('getProjects.aspx')">Projects and DB's by Script</button>
   </div>
</div>
</asp:content>
<asp:content id="Content6" contentplaceholderid="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:content>
<asp:content id="Content7" contentplaceholderid="AddressVerification" runat="server">
</asp:content>
