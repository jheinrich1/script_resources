﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Script_Resources_2020
{
    public partial class table : System.Web.UI.Page
    {
        DataTable tb = new DataTable();
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                createtable();
            }




        }
        public void createtable()
        {
            //Create table columns
            tb.Columns.Add("Prod_NO", typeof(string));
            tb.Columns.Add("Prod_Name", typeof(string));
            tb.Columns.Add("Order_Date", typeof(string));
            tb.Columns.Add("Quantity", typeof(string));

            //Add row
            dr = tb.NewRow();
            dr["Prod_NO"] = "101";
            dr["Prod_Name"] = "Raspberries";
            dr["Order_Date"] = "12/12/2020";
            dr["Quantity"] = "80";
            tb.Rows.Add(dr);

            //Display table
            Gv1.DataSource = tb;
            Gv1.DataBind();
            ViewState["table1"] = tb;

        }

        protected void Addnewrow(object sender, EventArgs e)
        {
            tb = (DataTable)ViewState["table1"];

            dr = tb.NewRow();
            dr["Prod_NO"] = txtb1.Text;
            dr["Prod_Name"] = txtb2.Text;
            dr["Order_Date"] = txtb3.Text;
            dr["Quantity"] = txtb4.Text;
            tb.Rows.Add(dr);

            Gv1.DataSource = tb;
            Gv1.DataBind();

            txtb1.Text = "";
            txtb2.Text = "";
            txtb3.Text = "";
            txtb4.Text = "";
        }
    }
}