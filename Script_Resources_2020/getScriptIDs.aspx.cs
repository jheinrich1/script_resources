﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Web.UI.WebControls;
using System.Windows;

namespace Script_Resources_2020
{
    public partial class getScriptIDs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RenderScripts(sender, e);
            }
        }

        protected void RenderScripts(object sender, EventArgs e)
        {
            try
            {
                List<string> returnVal = new List<string>();
                var scriptList = Script.GetScripts();
                var sortedList = new List<Script>();
                sortedList = scriptList.OrderBy(a => a.ScriptName).ToList();

                foreach (var script in sortedList)
                {
                    returnVal.Add("<p class='extras'>" + script.ScriptName + " - " + script.ScriptID + "</p>");
                }

                fields.InnerHtml = "";
                foreach (string i in returnVal)
                {
                    fields.InnerHtml += i;
                }

                fieldSearch.Visible = true;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}