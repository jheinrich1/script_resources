﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace Script_Resources_2020
{
    public class Script
    {
        public int ScriptID { get; set; }
        public string ScriptName { get; set; }
        public static string Error { get; set; }
        public static List<Script> GetScripts()
        {
            var scripts = new List<Script>();
            Error = "";
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            using (var command = new SqlCommand("usp_GetScriptNamesAndKeys", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    conn.Open();
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var script = new Script
                            {
                                ScriptID = Convert.ToInt32(dr[0]),
                                ScriptName = dr[1].ToString(),
                            };
                            scripts.Add(script);
                        }
                        dr.Close();
                    }
                }
                catch (SqlException se)
                {
                    Error = se.Message;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                }
            }

            return scripts;
        }

        public static Script AddScriptToDB(string scriptFolderName)
        {
            Script script = new Script();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterStaging"].ConnectionString))
            using (var command = new SqlCommand("usp_ScriptEditor_AddScriptFolderToSystem", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@pFolderName", SqlDbType.NVarChar)).Value = scriptFolderName;
                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();

                } catch (SqlException se)
                {
                    Error=se.Message;
                } catch (Exception ex)
                {
                    Error=ex.Message;
                }
            }
            return script;
        }
    }
}