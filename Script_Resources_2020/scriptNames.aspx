﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="scriptNames.aspx.cs" Inherits="Script_Resources_2020.scriptNames" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="BannerName" runat="server">
    
</asp:content>
<asp:content id="Content3" contentplaceholderid="MenuLinkHolder" runat="server">
   <div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">Common JS Functions</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                    <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                    <a class="nav-item nav-link" href="Dbclean.aspx">Db Cleaner</a>
                    <a class="nav-item nav-link" href="DateTimePicker.aspx">Date and Time Picker</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:content>
<asp:content id="Content4" contentplaceholderid="PageNameHolder" runat="server">
   
</asp:content>
<asp:content id="Content5" contentplaceholderid="Content" runat="server">
<div class="container">
    <div class="row">
        <p>
            Get dispos for the selected campaign
        </p>
        <asp:DropDownList ID="listOfCampaigns"
                    AutoPostBack="False"
                    runat="server"
                    OnSelectedIndexChanged="getDisposForSelectedCampaign">

               </asp:DropDownList>
    </div>
    <div class="row">
        <div class="col-3">
            <h1>
                Scripts
            </h1>
            <div id="namesOfScripts" runat="server">
       
            </div>
        </div>
        <div class="col-3">
            <h1>
                Campaigns
            </h1>
            <div id="namesOfCampaigns" runat="server">

            </div>
        </div>
        <div class="col-3">
            <h1>
                Dispositions By Campaign
            </h1>
            <div id="disposByCampaign" runat="server">

            </div>
        </div>
        <div class="col-3">
            <h1>
                Scripts By Campaign
            </h1>
            <div id="scriptsByCampaign" runat="server">

            </div>
        </div>
    </div>
</div>
</asp:content>
<asp:content id="Content6" contentplaceholderid="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:content>
<asp:content id="Content7" contentplaceholderid="AddressVerification" runat="server">
</asp:content>
