﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Script_Resources_2020
{
    public partial class linq : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable ShipmentData = Tracking.GetAllRecords();

            foreach (DataRow row in ShipmentData.Rows)
            {
                TableRow tableRow = new TableRow();

                foreach (DataColumn column in ShipmentData.Columns)
                {
                    // Create a new TableCell for this column of data
                    TableCell tableCell = new TableCell();

                    // Set the text of the TableCell to the value of this cell in the DataTable
                    tableCell.Text = row[column].ToString();

                    // Add the TableCell to the TableRow
                    tableRow.Cells.Add(tableCell);
                }

                ShipmentData.Rows.Add(tableRow);
            }
        }
    }
}