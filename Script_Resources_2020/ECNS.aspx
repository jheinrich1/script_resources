﻿<%@ Page Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="ECNS.aspx.cs" Inherits="Script_Resources_2020.ECNS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BannerName" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MenuLinkHolder" runat="server">
    <div id="navigationHeader">
        <nav class="navbar navbar-expand-lg navbarHeader">
            <div class="container headerContainer">
                <a class="navbar-brand" href="#">Get Dispositions</a>
            </div>
        </nav>
    </div>
    <div id="navigationBar">
        <nav class="navbar navbar-expand-lg">
            <div class="container navContainer">
                <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="commonfields.aspx">Amcat Fields</a>
                        <a class="nav-item nav-link" href="clean.aspx">Dispo Cleaner</a>
                        <a class="nav-item nav-link" href="DateTimePicker.aspx">Date and Time Picker</a>
                        <a class="nav-item nav-link" href="js.aspx">JavaScript</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageNameHolder" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <a href="../Regence_Medigap_UT_Enrollment_2023/start.aspx">Enrollment test</a>
    <div class="container">
        <select id="categoryDropdown">
            <option value="">Dispositions</option>
        </select>
        <br />
        <hr />
        <div id="categoryContainer">
        </div>
        <br />
        <hr />
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>

    </div>

    <script>
        $(document).ready(function () {
            getDispositions("1250");
        })
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="AddressVerification" runat="server">
</asp:Content>
