﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Resources.Master" AutoEventWireup="true" CodeBehind="createScript.aspx.cs" Inherits="Script_Resources_2020.createScript" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType virtualpath="~/Resources.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:content id="Content2" contentplaceholderid="BannerName" runat="server">
    
</asp:content>
<asp:content id="Content3" contentplaceholderid="MenuLinkHolder" runat="server">
   <div id="navigationHeader">
    <nav class="navbar navbar-expand-lg navbarHeader">
        <div class="container headerContainer">
            <a class="navbar-brand" href="#">Create Script</a>
        </div>
    </nav>
</div>
<div id="navigationBar">
    <nav class="navbar navbar-expand-lg">
        <div class="container navContainer">
            <%--<a class="navbar-brand" href="#">Dispo Cleaner</a>--%>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="Default.aspx">Home</a>
                    <a class="nav-item nav-link" href="getDispos.aspx">Dispositions by Campaign</a>
                    <a class="nav-item nav-link" href="getFields.aspx">Amcat Fields by Client</a>
                    <a class="nav-item nav-link" href="getProjects.aspx">Projects & DB's by Script</a>
                </div>
            </div>
        </div>
    </nav>
</div>
</asp:content>
<asp:content id="Content4" contentplaceholderid="PageNameHolder" runat="server">
   <input id="curUser" runat="server" />
</asp:content>
<asp:content id="Content5" contentplaceholderid="Content" runat="server">
   <div class="container">
       <labels style="display: block; color:white;">Script Name(s): </labels>
       <div class="col-6" style="padding-left: 0px; display:inline-flex;">
           <input id="scriptName" runat="server" class="form-control" style="margin-bottom:10px; padding-left:0px; width: 300px;"/>
           <span style="color:red; margin-left:3px; margin-top:-5px; font-weight:bold; font-size:16px;">*</span>
       </div>
       <div id="scriptContainer" style="display:grid">

       </div>
       <div style="display:inline-flex; margin-top: 10px">
           <button type="button" onclick="createEverything()" class="form-control">Create Script(s)</button>
           <img 
               src="icons/plus-white/white-add-new-50.png" 
               style="cursor:pointer; width: 36px; margin-left: 15px;" 
               onclick="createEntry()"
            />
       </div>
       
       <div style="margin-top: 5px;">
           <span id="result" style="color:white;"></span>
       </div>
       <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false"></asp:Label> 
   </div>
        
</asp:content>
<asp:content id="Content6" contentplaceholderid="HiddenFieldHolder" runat="server">
    <input id="setPageName" value="" type="hidden" />
    <input id="AddEnrollLoadEvents" value="no" type="hidden" />
</asp:content>
<asp:content id="Content7" contentplaceholderid="AddressVerification" runat="server">
</asp:content>
