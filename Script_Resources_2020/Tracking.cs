﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Web;

namespace Script_Resources_2020
{
    public class Tracking
    {
        public static DataTable GetAllRecords()
        {
            // Create a new DataTable to hold the results
            DataTable table = new DataTable();

            // Create a new SqlConnection using the provided connection string
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ITServices"].ConnectionString))
            {
                IntPtr userToken = IntPtr.Zero;
                bool success = LogonUser("jheinrich", "RMGCOM", "Lesscoffee!", LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out userToken);
                if (!success)
                {
                    throw new ApplicationException("Failed to impersonate user.");
                }
                WindowsIdentity identity = new WindowsIdentity(userToken);
                WindowsImpersonationContext context = identity.Impersonate();
                // Open the connection
                connection.Open();

                // Create a new SqlCommand using inline SQL to select all records from the specified table
                using (SqlCommand command = new SqlCommand($"SELECT * FROM IS_NewHire_Tracking", connection))
                {
                    // Create a new SqlDataAdapter to fill the DataTable with the results of the query
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        // Fill the DataTable with the results of the query
                        adapter.Fill(table);
                    }
                }

                // Close the connection
                connection.Close();
            }

            // Return the populated DataTable
            return table;
        }

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(
            string lpszUsername,
            string lpszDomain,
            string lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            out IntPtr phToken);

        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_PROVIDER_DEFAULT = 0;
    }
}
