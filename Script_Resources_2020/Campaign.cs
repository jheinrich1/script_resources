﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace Script_Resources_2020
{
    public class Campaign
    {
        public string ScriptName { get; set; }
        public string CampaignName { get; set; }
        public string DatabaseName { get; set; }
        public static string Error { get; set; }

        public static List<Campaign> GetCampaignsAndDBs()
        {
            var campaigns = new List<Campaign>();
            Error = "";
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            using (var command = new SqlCommand("usp_cScriptEditor_GetAllPoolsAndDBNames", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                try
                {
                    conn.Open();
                    using (var dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var campaign = new Campaign
                            {
                                CampaignName = dr["Name"].ToString(),
                                DatabaseName = dr["DatabaseName"].ToString()
                            };
                            campaigns.Add(campaign);
                        }
                        dr.Close();
                    }
                }
                catch (SqlException se)
                {
                    Error = se.Message;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                }
            }

            return campaigns;
        }

        public static List<Campaign> GetCampaignsByScriptName(string scriptName)
        {
            var campaigns = new List<Campaign>();

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CallCenterConnectionString"].ConnectionString))
            {
                var cmd = new SqlCommand("usp_cScriptEditor_PoolScriptGroupListByScriptName", conn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new SqlParameter("@ScriptName", SqlDbType.NVarChar)).Value = scriptName;

                try
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var campaign = new Campaign
                            {
                                ScriptName = dr[0].ToString(),
                                CampaignName = dr[1].ToString()
                            };
                            campaigns.Add(campaign);
                        }
                    }
                }
                catch (Exception)
                {
                    return campaigns;
                }

            }
            return campaigns;

        }
    }
}